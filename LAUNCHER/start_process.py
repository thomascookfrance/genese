import os
import sys
from datetime import datetime



def     start_dump():
    
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/DUMPS/")
    print("{}{}".format("    debut telechargement des dumps", datetime.now()))
    os.system("python3 get_nurvis_dumps.py")
    print("{}{}".format("    fin telechargement des dumps", datetime.now()))
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/DUMPS/OP-COPROD/")
    print("{}{}".format("    debut des op", datetime.now()))
    os.system("python3 op_auc_vigimilia.py")
    print("                                            AUCHAN OK")
    os.system("python3 op_car_vigimilia.py")
    print("                                            CARREFOUR OK")
    os.system("python3 op_hav_vigimilia.py")
    print("                                            HAVAS OK")
    os.system("python3 op_lec_vigimilia.py")
    print("                                            LECLERC OK")
    print("{}{}".format("    fin des op", datetime.now()))
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/DUMPS/")
    print("{}{}".format("    debut concatenation des dumps", datetime.now()))
    os.system("python3 write_file_nef.py")
    print("{}{}".format("    fin concatenation des dumps", datetime.now()))
                 


def     start_xml():
    
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/XML/CMI24/")
    print("{}{}".format("    debut dl xml CMI", datetime.now()))
    os.system("python3 CMI24_xml_download.py")
    print("{}{}".format("    fin dl xml CMI", datetime.now()))
    print("{}{}".format("    nettoyage xml CMI", datetime.now()))
    os.system("python3 Xml_remove.py")
    print("{}{}".format("    fin nettoyage xml CMI", datetime.now()))
    print("{}{}".format("    xml CMI en step_v3", datetime.now()))
    os.system("python3 start_generation_xml_cmi.py")
    print("{}{}".format("    fin xml CMI en step_v3", datetime.now()))
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/XML/SYSPAD/")
    print("{}{}".format("    debut dl xml SYSPAD", datetime.now()))
    os.system("python3 Get_SYS_XML.py")
    print("{}{}".format("    fin dl xml SYSPAD", datetime.now()))
    print("{}{}".format("    nettoyage xml SYSPAD", datetime.now()))
    os.system("python3 delete_obselete_xml.py")
    print("{}{}".format("    xml SYSPAD en step_v3", datetime.now()))
    os.system("python3 start_generation_xml_sys.py")
    print("{}{}".format("    fin xml SYSPAD en step_v3", datetime.now()))
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/XML/SEPARATION/")
    print("{}{}".format("    mise a disposition des xml pour les partenaires", datetime.now()))
    os.system("python3 split_source_file.py")
    print("{}{}".format("    fin mise a disposition des xml pour les partenaires", datetime.now()))
    os.system("./zip_xml_orchestra.sh")
    print("{}{}".format("    fin mise a disposition des zip xml pour orchestra", datetime.now()))
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/XML/REMOTECONFIG/")
    os.system("python3 remote_config.py")
    os.system("./push_orchestra_remote.sh")
                    

def     start_image():
    
    os.chdir("/home/olivier/PROG_GENESE/PROGRAMMES/IMAGES/")
    print("{}{}".format("    dl des images CMI24", datetime.now()))
    os.system("python3 CMI_picture_dl.py")
    print("{}{}".format("    fin dl des images CMI24", datetime.now()))
    print("{}{}".format("    dl des images SYSPAD", datetime.now()))
    os.system("python3 pictures_SYSPAD.py")
    print("{}{}".format("    fin dl des images SYSPAD", datetime.now()))
    print("{}{}".format("    envoie des images sur static-jettour", datetime.now()))
    os.system("python3 resyze_picture.py")
    print("{}{}".format("    fin envoie des images sur static-jettour", datetime.now()))



print("{}{}".format("DEBUT DUMPS", datetime.now()))
start_dump()
print("{}{}".format("DEBUT XML", datetime.now()))
start_xml()
print("{}{}".format("DEBUT IMAGE", datetime.now()))
start_image()

