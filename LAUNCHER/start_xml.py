import os
import sys
from datetime import datetime




def     start_xml():
    
    os.chdir("/srv/Software/PROG_GENESE/PROGRAMMES/XML/CMI24/")
    print("{}{}".format("    debut dl xml CMI", datetime.now()))
    os.system("python3 CMI24_xml_download.py")
    print("{}{}".format("    fin dl xml CMI", datetime.now()))
    print("{}{}".format("    xml CMI en step_v3", datetime.now()))
    os.system("python3 start_generation_xml_cmi.py")
    print("{}{}".format("    fin xml CMI en step_v3", datetime.now()))
    os.chdir("/srv/Software/PROG_GENESE/PROGRAMMES/XML/SYSPAD/")
    print("{}{}".format("    debut dl xml SYSPAD", datetime.now()))
    os.system("python3 Get_SYS_XML.py")
    print("{}{}".format("    fin dl xml SYSPAD", datetime.now()))
    print("{}{}".format("    nettoyage xml SYSPAD", datetime.now()))
    os.chdir("/srv/Software/PROG_GENESE/PROGRAMMES/XML/CLEAN_TOOL/")
    os.system("python3 delete_obselete_xml.py")
    print("{}{}".format("    xml SYSPAD en step_v3", datetime.now()))
    os.chdir("/srv/Software/PROG_GENESE/PROGRAMMES/XML/SYSPAD/")
    os.system("python3 start_generation_xml_sys.py")
    print("{}{}".format("    fin xml SYSPAD en step_v3", datetime.now()))
    os.chdir("/srv/Software/PROG_GENESE/PROGRAMMES/XML/SEPARATION/")
    print("{}{}".format("    mise a disposition des xml pour les partenaires", datetime.now()))
    os.system("python3 split_source_file.py")
    print("{}{}".format("    fin mise a disposition des xml pour les partenaires", datetime.now()))
    os.system("./zip_xml_orchestra.sh")
    print("{}{}".format("    fin mise a disposition des zip xml pour orchestra", datetime.now()))
    os.chdir("/srv/Software/PROG_GENESE/PROGRAMMES/XML/REMOTECONFIG/")
    os.system("python3 remote_config.py")
    os.system("./push_ftpxt.sh")



    
print("{}{}".format("DEBUT XML", datetime.now()))
start_xml()
print("{}{}".format("FIN XML", datetime.now()))

