import os
import sys
import codecs
import gzip
import shutil
import config_op as CO
from zipfile import ZipFile



def     split_angebote_pauschal():

    f = open('AUC-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    y = open('OP-AUC-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    with open('AUC-ANGEBOTE-PAUSCHAL', 'rb') as file_list:
        for line in file_list:
            if b"NAUC" in line:
                f.write(bytearray(line))
            elif b"NECH" in line:
                y.write(bytearray(line))
        os.remove('AUC-ANGEBOTE-PAUSCHAL')
        os.rename("AUC-ANGEBOTE-PAUSCHAL.TXT", "AUC-ANGEBOTE-PAUSCHAL")
        os.rename("OP-AUC-ANGEBOTE-PAUSCHAL.TXT", "OP-AUC-ANGEBOTE-PAUSCHAL")
        with open("AUC-ANGEBOTE-PAUSCHAL", 'rb') as f_in:
            with gzip.open("AUC-ANGEBOTE-PAUSCHAL.gz", 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        with open("OP-AUC-ANGEBOTE-PAUSCHAL", 'rb') as f_in:
            with gzip.open("OP-AUC-ANGEBOTE-PAUSCHAL.gz", 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)




def     split_befo_verf():
    f = open('AUC-BEFO-VERF.txt', 'wb')
    y = open('OP-AUC-BEFO-VERF.txt', 'wb')
    with open('AUC-BEFO-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NAUC" in line:
                f.write(bytearray(line))
            elif b"NECH" in line:
                y.write(bytearray(line))
        os.remove('AUC-BEFO-VERF.TXT')
        os.rename("AUC-BEFO-VERF.txt", "AUC-BEFO-VERF.TXT")
        os.rename("OP-AUC-BEFO-VERF.txt", "OP-AUC-BEFO-VERF.TXT")
        os.system("zip AUC-BEFO-VERF.zip AUC-BEFO-VERF.TXT")
        os.system("zip OP-AUC-BEFO-VERF.zip OP-AUC-BEFO-VERF.TXT")


def     split_hotel_verf():
    f = open('AUC-HOTEL-VERF.txt', 'wb')
    y = open('OP-AUC-HOTEL-VERF.txt', 'wb')
    with open('AUC-HOTEL-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NAUC" in line:
                f.write(bytearray(line))
            elif b"NECH" in line:
                y.write(bytearray(line))
        os.remove('AUC-HOTEL-VERF.TXT')
        os.rename("AUC-HOTEL-VERF.txt", "AUC-HOTEL-VERF.TXT")
        os.rename("OP-AUC-HOTEL-VERF.txt", "OP-AUC-HOTEL-VERF.TXT")
        os.system("zip AUC-HOTEL-VERF.zip AUC-HOTEL-VERF.TXT")
        os.system("zip OP-AUC-HOTEL-VERF.zip OP-AUC-HOTEL-VERF.TXT")



def     init_dumps():
    os.chdir(CO.vigimilia_AUC_path)
    listing = os.listdir(CO.vigimilia_AUC_path)
    for elem in listing:
        if ".zip" in elem or ".gz" in elem :
            file_name = elem
            os.rename(CO.vigimilia_AUC_path + file_name, CO.tmp_AUC_path + file_name)
    os.chdir(CO.tmp_AUC_path)
    for elem in os.listdir(CO.tmp_AUC_path):
        if ".gz" in elem:
            os.system("gunzip " + elem)
        elif ".zip" in elem :
            os.system("unzip " + elem)
            os.remove(elem)
    split_angebote_pauschal()
    split_hotel_verf()
    split_befo_verf()
    for elem in os.listdir(CO.tmp_AUC_path):
        if ".TXT" in elem:
            os.remove(elem)
        elif ".gz" in elem or ".zip" in elem :
            os.rename(CO.tmp_AUC_path + elem, CO.vigimilia_AUC_path + elem)


init_dumps()
