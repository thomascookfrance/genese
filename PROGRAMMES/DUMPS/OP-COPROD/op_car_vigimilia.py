import os
import sys
import codecs
import gzip
import shutil
import config_op as CO
from zipfile import ZipFile



def     split_angebote_pauschal():

    f = open('CAR-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    y = open('OP-CAR-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    with open('CAR-ANGEBOTE-PAUSCHAL', 'rb') as file_list:
        for line in file_list:
            if b"NCRF" in line:
                f.write(bytearray(line))
            elif b"NECF" in line:
                y.write(bytearray(line))
        os.remove('CAR-ANGEBOTE-PAUSCHAL')
        os.rename("CAR-ANGEBOTE-PAUSCHAL.TXT", "CAR-ANGEBOTE-PAUSCHAL")
        os.rename("OP-CAR-ANGEBOTE-PAUSCHAL.TXT", "OP-CAR-ANGEBOTE-PAUSCHAL")


def     split_angebote_rund():
    f = open('CAR-ANGEBOTE-RUND.TXT', 'wb')
    y = open('OP-CAR-ANGEBOTE-RUND.TXT', 'wb')
    with open('CAR-ANGEBOTE-RUND', 'rb') as file_list:
        for line in file_list:
            if b"NCRF" in line:
                f.write(bytearray(line))
            elif b"NECF" in line:
                y.write(bytearray(line))
        os.remove('CAR-ANGEBOTE-RUND')
        os.rename("CAR-ANGEBOTE-RUND.TXT", "CAR-ANGEBOTE-RUND")
        os.rename("OP-CAR-ANGEBOTE-RUND.TXT", "OP-CAR-ANGEBOTE-RUND")



def     split_befo_verf():
    f = open('CAR-BEFO-VERF.txt', 'wb')
    y = open('OP-CAR-BEFO-VERF.txt', 'wb')
    with open('CAR-BEFO-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NCRF" in line:
                f.write(bytearray(line))
            elif b"NECF" in line:
                y.write(bytearray(line))
        os.remove('CAR-BEFO-VERF.TXT')
        os.rename("CAR-BEFO-VERF.txt", "CAR-BEFO-VERF.TXT")
        os.rename("OP-CAR-BEFO-VERF.txt", "OP-CAR-BEFO-VERF.TXT")


def     split_hotel_verf():
    f = open('CAR-HOTEL-VERF.txt', 'wb')
    y = open('OP-CAR-HOTEL-VERF.txt', 'wb')
    with open('CAR-HOTEL-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NCRF" in line:
                f.write(bytearray(line))
            elif b"NECF" in line:
                y.write(bytearray(line))
        os.remove('CAR-HOTEL-VERF.TXT')
        os.rename("CAR-HOTEL-VERF.txt", "CAR-HOTEL-VERF.TXT")
        os.rename("OP-CAR-HOTEL-VERF.txt", "OP-CAR-HOTEL-VERF.TXT")



def     init_dumps():
    os.chdir(CO.vigimilia_CAR_path)
    listing = os.listdir(CO.vigimilia_CAR_path)
    for elem in listing:
        if ".zip" in elem or ".gz" in elem :
            file_name = elem
            os.rename(CO.vigimilia_CAR_path + file_name, CO.tmp_CAR_path + file_name)
    os.chdir(CO.tmp_CAR_path)
    for elem in os.listdir(CO.tmp_CAR_path):
        if ".gz" in elem:
            os.system("gunzip " + elem)
        elif ".zip" in elem :
            os.system("unzip " + elem)
            os.remove(elem)
    split_angebote_pauschal()
    split_angebote_rund()
    split_hotel_verf()
    split_befo_verf()
    



init_dumps()
