import os
import sys
import codecs
import shutil
import gzip
import config_op as CO
from zipfile import ZipFile


def     split_angebote_pauschal():
    f = open('HAV-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    y = open('OP-HAV-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    with open('HAV-ANGEBOTE-PAUSCHAL', 'rb') as file_list:
        for line in file_list:
            if b"NEHV" in line:
                f.write(bytearray(line))
            elif b"NHVS" in line:
                y.write(bytearray(line))
        os.remove('HAV-ANGEBOTE-PAUSCHAL')
        os.rename("HAV-ANGEBOTE-PAUSCHAL.TXT", "HAV-ANGEBOTE-PAUSCHAL")
        os.rename("OP-HAV-ANGEBOTE-PAUSCHAL.TXT", "OP-HAV-ANGEBOTE-PAUSCHAL")
        with open("HAV-ANGEBOTE-PAUSCHAL", 'rb') as f_in:
            with gzip.open("HAV-ANGEBOTE-PAUSCHAL.gz", 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        with open("OP-HAV-ANGEBOTE-PAUSCHAL", 'rb') as f_in:
            with gzip.open("OP-HAV-ANGEBOTE-PAUSCHAL.gz", 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)


def     split_befo_verf():
    f = open('HAV-BEFO-VERF.txt', 'wb')
    y = open('OP-HAV-BEFO-VERF.txt', 'wb')
    with open('HAV-BEFO-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NEHV" in line:
                f.write(bytearray(line))
            elif b"NHVS" in line:
                y.write(bytearray(line))
        os.remove('HAV-BEFO-VERF.TXT')
        os.rename("HAV-BEFO-VERF.txt", "HAV-BEFO-VERF.TXT")
        os.rename("OP-HAV-BEFO-VERF.txt", "OP-HAV-BEFO-VERF.TXT")
        os.system("zip HAV-BEFO-VERF.zip HAV-BEFO-VERF.TXT")
        os.system("zip OP-HAV-BEFO-VERF.zip OP-HAV-BEFO-VERF.TXT")


def     split_hotel_verf():
    f = open('HAV-HOTEL-VERF.txt', 'wb')
    y = open('OP-HAV-HOTEL-VERF.txt', 'wb')
    with open('HAV-HOTEL-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NEHV" in line:
                f.write(bytearray(line))
            elif b"NHVS" in line:
                y.write(bytearray(line))
        os.remove('HAV-HOTEL-VERF.TXT')
        os.rename("HAV-HOTEL-VERF.txt", "HAV-HOTEL-VERF.TXT")
        os.rename("OP-HAV-HOTEL-VERF.txt", "OP-HAV-HOTEL-VERF.TXT")
        os.system("zip HAV-HOTEL-VERF.zip HAV-HOTEL-VERF.TXT")
        os.system("zip OP-HAV-HOTEL-VERF.zip OP-HAV-HOTEL-VERF.TXT")



def     init_dumps():
    os.chdir(CO.vigimilia_HAV_path)
    listing = os.listdir(CO.vigimilia_HAV_path)
    for elem in listing:
        if ".zip" in elem or ".gz" in elem :
            file_name = elem
            os.rename(CO.vigimilia_HAV_path + file_name, CO.tmp_HAV_path + file_name)
    os.chdir(CO.tmp_HAV_path)
    for elem in os.listdir(CO.tmp_HAV_path):
        if ".gz" in elem:
            os.system("gunzip " + elem)
        elif ".zip" in elem :
            os.system("unzip " + elem)
            os.remove(elem)
    split_angebote_pauschal()
    split_hotel_verf()
    split_befo_verf()
    for elem in os.listdir(CO.tmp_HAV_path):
        if ".TXT" in elem:
            os.remove(elem)
        elif ".gz" in elem or ".zip" in elem :
            os.rename(CO.tmp_HAV_path + elem, CO.vigimilia_HAV_path + elem)
    



init_dumps()
