import os
import sys
import gzip
import shutil
import codecs
import subprocess
import config_op as CO
from zipfile import ZipFile



def     split_angebote_pauschal():
    
    f = open('LEC-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    y = open('OP-LEC-ANGEBOTE-PAUSCHAL.TXT', 'wb')
    
    with open('LEC-ANGEBOTE-PAUSCHAL', 'rb') as file_list:
        for line in file_list:
            if b"NEFC" in line:
                f.write(bytearray(line))
            elif b"NECL" in line:
                y.write(bytearray(line))
        os.remove('LEC-ANGEBOTE-PAUSCHAL')
        os.rename("LEC-ANGEBOTE-PAUSCHAL.TXT", "LEC-ANGEBOTE-PAUSCHAL")
        os.rename("OP-LEC-ANGEBOTE-PAUSCHAL.TXT", "OP-LEC-ANGEBOTE-PAUSCHAL")
                                                        
        

def     split_angebote_rund():
    
    f = open('LEC-ANGEBOTE-RUND.TXT', 'wb')
    y = open('OP-LEC-ANGEBOTE-RUND.TXT', 'wb')
    
    with open('LEC-ANGEBOTE-RUND', 'rb') as file_list:
        for line in file_list:
            if b"NEFC" in line:
                f.write(bytearray(line))
            elif b"NECL" in line:
                y.write(bytearray(line))
        os.remove('LEC-ANGEBOTE-RUND')
        os.rename("LEC-ANGEBOTE-RUND.TXT", "LEC-ANGEBOTE-RUND")
        os.rename("OP-LEC-ANGEBOTE-RUND.TXT", "OP-LEC-ANGEBOTE-RUND")


def     split_hotel_verf():

    import glob
    f = open('LEC-HOTEL-VERF.txt', 'wb')
    y = open('OP-LEC-HOTEL-VERF.TXT', 'wb')    
    with open('LEC-HOTEL-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NEFC" in line:
                f.write(bytearray(line))
            elif b"NECL" in line:
                y.write(bytearray(line))
        os.remove("LEC-HOTEL-VERF.TXT")
        os.rename("LEC-HOTEL-VERF.txt", "LEC-HOTEL-VERF.TXT")
        
    
def     split_befo_verf():
    
    f = open('LEC-BEFO-VERF.txt', 'wb')
    y = open('OP-LEC-BEFO-VERF.txt', 'wb')
    with open('LEC-BEFO-VERF.TXT', 'rb') as file_list:
        for line in file_list:
            if b"NEFC" in line:
                f.write(bytearray(line))
            elif b"NECL" in line:
                y.write(bytearray(line))
        os.remove('LEC-BEFO-VERF.TXT')
        os.rename("LEC-BEFO-VERF.txt", "LEC-BEFO-VERF.TXT")
        os.rename("OP-LEC-BEFO-VERF.txt", "OP-LEC-BEFO-VERF.TXT")

    
def     init_dumps():
    
    os.chdir(CO.vigimilia_LEC_path)
    listing = os.listdir(CO.vigimilia_LEC_path)
    for elem in listing:
        if ".zip" in elem or ".gz" in elem :
            file_name = elem
            os.rename(CO.vigimilia_LEC_path + file_name, CO.tmp_LEC_path + file_name)
    os.chdir(CO.tmp_LEC_path)
    for elem in os.listdir(CO.tmp_LEC_path):
        if ".gz" in elem:
            os.system("gunzip " + elem)
        elif ".zip" in elem :
            os.system("unzip " + elem)
            os.remove(elem)
    split_angebote_pauschal()
    split_angebote_rund()
    split_hotel_verf()
    split_befo_verf()
    



init_dumps()
