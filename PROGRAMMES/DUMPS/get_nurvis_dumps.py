import os
import sys
import gzip
import zipfile
import shutil
import config_dumps as CD
from ftplib import FTP
from contextlib import closing
import urllib.request as urllib2

proxy = urllib2.ProxyHandler({'ftp': 'http://172.25.4.57:3128'})
opener = urllib2.build_opener(proxy)
url = "ftp://tcfrankreich:0eMADvfj@ftpdataserver.thomascook.de"
url_dyn = "ftp://tcdynfrankreich:nODqDJVW@ftpdataserver.thomascook.de/NEFY/AICP/PROD"



#------:met en list les dumps classic a partir du fichier de configuration
def     list_classic_file():

    list_file = []
    with open("dumps_nurvis_classic.txt") as data:
        for line in data :
            list_file.append(line[:-1])
        return (list_file)

#------:met en list les dumps dynamique a partir du fichier de configuration
def     list_dyn_file():

    list_file_dyn = []
    with open("dumps_nurvis_NEY.txt") as data:
        for filename in data :
            list_file_dyn.append(filename[:-1])
        return (list_file_dyn)

#------:telechargement des dumps classic
def     classic_dumps() :

    listing = list_classic_file()
    with FTP(CD.nurvis_server) as ftp:
        ftp.login(CD.classic_user, CD.classic_password)
        listf = ftp.nlst()
        for elem in listing:
            if elem in listf:
                filename = elem
                flux_log.write(filename + " DOWNLOAD\n")
                fl = open(filename, 'wb')
                ftp.retrbinary('RETR ' + filename, fl.write)


#------:telechargement des dumps dynamique
def     dyn_dumps():

    listing = list_dyn_file()
    with FTP(CD.nurvis_server) as ftp:
        ftp.login(CD.dyn_user, CD.dyn_password)
        ftp.cwd('/NEFY/AICP/PROD/')
        listf = ftp.nlst()
        for elem in listing:
            if elem in listf:
                filename = elem
                flux_log.write(filename + " DOWNLOAD\n")
                fl = open(filename, 'wb')
                ftp.retrbinary('RETR ' + filename, fl.write)

#------:telechargement des dumps classic via le proxy
def     classic_dumps_proxy() :

    listing = list_classic_file()
    for elem in listing:
        try:
            with closing(opener.open(url + "/" + elem)) as r:
                with open(elem, 'wb') as target:
                    target.write(r.read())
        except:
            flux_log.write(elem + " Is not on the server\n")


#------:telechargement des dumps dynamique via le proxy
def     dyn_dumps_proxy():

    listing = list_dyn_file()
    for elem in listing:
        try:
            with closing(opener.open(url_dyn + "/" + elem)) as r:
                with open(elem, 'wb') as target:
                    target.write(r.read())
        except:
            flux_log.write(elem + " Is not on the server\n")



#------:Verifie la qualite des dumps
def     check_quality_file():

    listing = os.listdir(CD.path_script)
    for elem in listing:
        if ".gz" in elem:
            chkgz = os.system("gzip -t " + elem)
            if chkgz == 0:
                os.rename(CD.path_script + elem, CD.path_dest + elem)
        elif ".zip" in elem:
            zip_file = zipfile.ZipFile(elem)
            chkzip = zip_file.testzip()
            if not chkzip :
                os.rename(CD.path_script + elem, CD.path_dest + elem)

#------:nettoie les repertoires
def     clean_path(path):
    os.chdir(path)
    list = os.listdir(path)
    if len(list) > 1:
        for elem in list:
            if "gz" in elem or "zip" in elem or ".TXT" in elem:
                os.remove(elem)
            else:
                programme_log.write(path + " EMPTY : no gz or zip or txt files\n")
    else:
        programme_log.write(path + " EMPTY\n")


#------:nettoie les repertoires tmp
def     clean_path_tmp(path):
    os.chdir(path)
    list = os.listdir(path)
    if len(list) > 0:
        for elem in list:
            os.remove(elem)
    else:
        programme_log.write(path + " EMPTY\n")


#------:nettoie les repertoires du partenaire vigimilia pour les op
def     clean():

    programme_log.write("NETTOYAGE de AUC\n")
    clean_path(CD.tmp_vigimilia + "AUC/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "AUC/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de JBO\n")
    clean_path(CD.tmp_vigimilia + "JBO/NURVIS/")
    programme_log.write("NETTOYAGE de JET\n")
    clean_path(CD.tmp_vigimilia + "JET/NURVIS/")
    programme_log.write("NETTOYAGE de LID\n")
    clean_path(CD.tmp_vigimilia + "LID/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "LID/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de LST\n")
    clean_path(CD.tmp_vigimilia + "LST/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "LST/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de NFJ\n")
    clean_path(CD.tmp_vigimilia + "NFJ/NURVIS/")
    programme_log.write("NETTOYAGE de SEL\n")
    clean_path(CD.tmp_vigimilia + "SEL/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "SEL/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de CAR\n")
    clean_path(CD.tmp_vigimilia + "CAR/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "CAR/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de HAV\n")
    clean_path(CD.tmp_vigimilia + "HAV/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "HAV/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de LEC\n")
    clean_path(CD.tmp_vigimilia + "LEC/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "LEC/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de PRV\n")
    clean_path(CD.tmp_vigimilia + "PRV/NURVIS/")
    clean_path_tmp(CD.tmp_vigimilia + "PRV/NURVIS/TMP/")
    programme_log.write("NETTOYAGE de VP\n")
    clean_path(CD.tmp_vigimilia + "VP/NURVIS/")


#------:envoie les dumps a tout les partenaires + monitoring

def     dispatch_file():

    clean()
    os.chdir(CD.path_dest)
    list_file = os.listdir(CD.path_dest)
    for elem in list_file :
        flux_log.write(elem + " SEND\n")
        shutil.copy2(CD.path_dest + elem, CD.tmp_monitoring + elem)
        if "CODES" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "ALL/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_orchestra_code + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "ALL/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_onparou + "ALL/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_speedmedia + "ALL/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_qcnscruise + "ALL/NURVIS/" + elem)
        if "AUC" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "AUC/NURVIS/" + elem)
        if "HAV" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "HAV/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "HAV/NURVIS/" + elem)
        if "CAR" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "CAR/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "CAR/NURVIS/" + elem)
        if "JBO" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "JBO/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "JBO/NURVIS/" + elem)
        if "LEC" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "LEC/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "LEC/NURVIS/" + elem)
        if "LID" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "LID/NURVIS/" + elem)
        if "LST" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "LST/NURVIS/" + elem)
        if "NFJ" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "NFJ/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "JET/NURVIS/" + elem)
        if "PRV" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "PRV/NURVIS/" + elem)
        if "SEL" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "SEL/NURVIS/" + elem)
        if "VP" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "VP/NURVIS/" + elem)
        if "AQT" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_orchestra + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "AQT/NURVIS/" + elem)
        if "NEF" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_orchestra + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_vigimilia + "JET/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "JET/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_onparou + "JET/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_speedmedia + "JET/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_qcnscruise + "JET/NURVIS/" + elem)
        if "NEY" in elem:
            shutil.copy2(CD.path_dest + elem, CD.tmp_orchestra + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_travel + "JET/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_onparou + "JET/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_speedmedia + "JET/NURVIS/" + elem)
            shutil.copy2(CD.path_dest + elem, CD.tmp_qcnscruise + "JET/NURVIS/" + elem)
        if "NFU" in elem :
            shutil.copy2(CD.path_dest + elem, CD.tmp_orchestra + elem)
        if "TCI" in elem :
            shutil.copy2(CD.path_dest + elem, CD.tmp_pl_orchestra + elem)
        if "FCY" in elem :
            shutil.copy2(CD.path_dest + elem, CD.tmp_pl_orchestra + elem)


#------:verifie les dumps AQT. si erreur envoie les dumps AQT de la veille
def     back_up_aqt():

    os.chdir(CD.path_dest)
    list_file = os.listdir(CD.path_dest)
    l = []
    files_ref = ["AQT-ANGEBOTE-PAUSCHAL-D07.gz", "AQT-ANGEBOTE-PAUSCHAL-D14.gz",
                 "AQT-BEFO-VERF.zip", "AQT-HOTEL-VERF.zip"]
    for elem in list_file :
        for elem2 in files_ref :
            if elem2 in elem :
                l.append(elem)
    if len(l) == 4:
        for elem in files_ref:
            shutil.copyfile(CD.path_dest + elem, CD.aqt_bk_path + elem)
    if len(l) != 4 :
        alert_log.write("SEND AQT BACK UP\n")
        os.chdir(CD.aqt_bk_path)
        for elem in os.listdir(CD.aqt_bk_path):
            shutil.copyfile(CD.aqt_bk_path + elem, CD.path_dest + elem)


#------:verifie les dumps NEF si erreur envoie les dumps NEF de la veille
def     back_up_nef():

    os.chdir(CD.path_dest)
    l = []
    list_file = os.listdir(CD.path_dest)
    files_ref = ["NEF-ANGEBOTE-PAUSCHAL-D07.gz", "NEF-ANGEBOTE-PAUSCHAL-D14.gz", "NEF-HOTEL-VERF.zip", "NEF-BEFO-VERF.zip", "NEF-ANGEBOTE-HOTEL-D07.gz", "NEF-ANGEBOTE-LOCATION.gz", "NEF-SONDERANGEBOTE.zip"]
    for elem in list_file:
        for elem2 in files_ref :
            if elem2 in elem :
                l.append(elem)
    if len(l) == 7:
        for elem in files_ref:
            shutil.copyfile(CD.path_dest + elem, CD.nef_bk_path + elem)
    if len(l) != 7 :
        alert_log.write("SEND NEF BACK UP\n")
        os.chdir(CD.nef_bk_path)
        for elem in os.listdir(CD.nef_bk_path):
            shutil.copyfile(CD.nef_bk_path + elem, CD.path_dest + elem)


#------:verifie les dumps NEY si erreur envoie les dumps NEY de la veille
def     back_up_ney():

    os.chdir(CD.path_dest)
    l = []
    list_file = os.listdir(CD.path_dest)
    files_ref = ["NEY-ANGEBOTE-PAUSCHAL.gz", "NEY2-ANGEBOTE-PAUSCHAL.gz", "NEY-BEFO-VERF.zip",
                 "NEY-HOTEL-VERF.zip", "NEY2-BEFO-VERF.zip", "NEY2-HOTEL-VERF.zip"]
    for elem in list_file:
        for elem2 in files_ref :
            if elem2 in elem :
                l.append(elem)
    if len(l) == 6:
        for elem in files_ref:
            shutil.copyfile(CD.path_dest + elem, CD.ney_bk_path + elem)
    if len(l) != 6 :
        alert_log.write("SEND NEY BACK UP\n")
        os.chdir(CD.ney_bk_path)
        for elem in os.listdir(CD.ney_bk_path):
            shutil.copyfile(CD.ney_bk_path + elem, CD.path_dest + elem)



#------:verifie les dumps NFJ si erreur envoie les dumps NFJ de la veille
def     back_up_nfj():

    os.chdir(CD.path_dest)
    l = []
    list_file = os.listdir(CD.path_dest)
    files_ref = ["NFJ-ANGEBOTE-PAUSCHAL-D07.gz", "NFJ-ANGEBOTE-RUND.gz",
                 "NFJ-BEFO-VERF.zip", "NFJ-HOTEL-VERF.zip"]
    for elem in list_file:
        for elem2 in files_ref :
            if elem2 in elem :
                l.append(elem)
    if len(l) == 4:
        for elem in files_ref:
            shutil.copyfile(CD.path_dest + elem, CD.nfj_bk_path + elem)
    if len(l) != 4 :
        alert_log.write("SEND NFJ BACK UP\n")
        os.chdir(CD.nfj_bk_path)
        for elem in os.listdir(CD.nfj_bk_path):
            shutil.copyfile(CD.nfj_bk_path + elem, CD.path_dest + elem)




#------:transformation des dumps pour Vente Prive
def     change_VP():

    dir_path = CD.tmp_vigimilia + 'VP/NURVIS/'
    os.chdir(dir_path)
    for elem in os.listdir(dir_path):
        if ".gz" in elem :
            os.system("gunzip " + elem)
        if ".zip" in elem:
            os.system("unzip " + elem)
    os.system("mv VP-ANGEBOTE-PAUSCHAL-P VP-ANGEBOTE-PAUSCHAL")
    os.system("mv VP-BEFO-VERF-P VP-BEFO-VERF.TXT")
    os.system("mv VP-HOTEL-VERF-P VP-HOTEL-VERF.TXT")
    os.system("gzip VP-ANGEBOTE-PAUSCHAL")
    os.system("zip VP-BEFO-VERF.zip VP-BEFO-VERF.TXT")
    os.system("zip VP-HOTEL-VERF.zip VP-HOTEL-VERF.TXT")
    for elem in os.listdir(dir_path):
        if ".TXT" in elem :
            os.remove(elem)


#------:transformation des dumps pour PRV
def     change_PRV():

    dir_path = CD.tmp_vigimilia + 'PRV/NURVIS/'
    os.chdir(dir_path)
    os.system("gunzip PRV-ANGEBOTE-PAUSCHAL.gz")
    os.system("unzip PRV-BEFO-VERF.zip")
    os.system("unzip PRV-HOTEL-VERF.zip")
    os.system("mv PRV-ANGEBOTE-PAUSCHAL OP-PRV-ANGEBOTE-PAUSCHAL")
    os.system("mv PRV-BEFO-VERF.TXT OP-PRV-BEFO-VERF.TXT")
    os.system("mv PRV-HOTEL-VERF.TXT OP-PRV-HOTEL-VERF.TXT")
    os.system("gzip OP-PRV-ANGEBOTE-PAUSCHAL")
    os.system("zip OP-PRV-BEFO-VERF.zip OP-PRV-BEFO-VERF.TXT")
    os.system("zip OP-PRV-HOTEL-VERF.zip OP-PRV-HOTEL-VERF.TXT")





#------: fichiers log
programme_log = open("../LOG/programme.log", 'w')
flux_log = open("../LOG/flux.log", 'w')
alert_log = open("../LOG/alert.log", 'w')

#----------------------------------------------------------------

programme_log.write("DEBUT DU TELECHARGEMENT DES DUMPS CLASSIC\n")
classic_dumps_proxy()
programme_log.write("FIN DU TELECHARGEMENT DES DUMPS CLASSIC => OK\n")
programme_log.write("DEBUT DU TELECHARGEMENT DES DUMPS PROD\n")
dyn_dumps_proxy()
programme_log.write("FIN DU TELECHARGEMENT DES DUMPS PROD => OK\n")
programme_log.write("DEBUT CHECK QUALITY DES DUMPS\n")
check_quality_file()
programme_log.write("FIN CHECK QUALITY DES DUMPS => OK\n")
programme_log.write("DEBUT CHECK FICHIERS OBLIGATOIRES\n")
programme_log.write("                        AQT BACK UP FILES\n")
back_up_aqt()
programme_log.write("                        AQT BACK UP FILES => OK\n")
programme_log.write("                        NEF BACK UP FILES\n")
back_up_nef()
programme_log.write("                        NEF BACK UP FILES => OK\n")
programme_log.write("                        NEY BACK UP FILES\n")
back_up_ney()
programme_log.write("                        NEY BACK UP FILES => OK\n")
programme_log.write("                        NFJ BACK UPFILES\n")
back_up_nfj()
programme_log.write("                        NFJ BACK UPFILES => OK\n")
programme_log.write("FIN CHECK FICHIERS OBLIGATOIRES => OK\n")
programme_log.write("DEBUT DISPATCH DES DUMPS\n")
dispatch_file()
programme_log.write("FIN DISPATCH DES DUMPS => OK\n")
programme_log.write("TRANSFO VP DUMPS\n")
change_VP()
programme_log.write("TRANSFO VP DUMPS => OK\n")
programme_log.write("TRANSFO PRV DUMPS\n")
change_PRV()
programme_log.write("TRANSFO PRV DUMPS => OK\n")
programme_log.close()
flux_log.close()
alert_log.close()
