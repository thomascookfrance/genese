import os
import sys
import shutil
import gzip
import config_dumps as CD




#--------concatenation des dumps NEF-ANGEBOTE-HOTEK-D** pour orchestra

def     nef_trt():

    os.chdir(CD.path_dest)
    listing = os.listdir(CD.path_dest)
    print(CD.path_dest)
    for elem in listing:
        if "NEF-ANGEBOTE-HOTEL-D" in elem :
            shutil.move(elem, CD.trt_nef_path + elem)


def     concat_nef():

    os.chdir(CD.trt_nef_path)
    listing = os.listdir(CD.trt_nef_path)
    with open("NEF-ANGEBOTE-HOTEL.TXT", 'wb') as data:
        for elem in listing:
            print("traitement de " + elem)
            with gzip.open(elem, 'rb') as f:
                file_data = f.read()
                data.write(file_data)
                f.close()
    data.close()
    shutil.make_archive("NEF-ANGEBOTE-HOTEL", "zip", ".", "NEF-ANGEBOTE-HOTEL.TXT")
    shutil.move("NEF-ANGEBOTE-HOTEL.zip",  CD.tmp_orchestra + "NEF-ANGEBOTE-HOTEL.zip")
    for elem in listing:
        os.remove(elem)
    os.remove("NEF-ANGEBOTE-HOTEL.TXT")
                                                                                                                                                                                            

nef_trt()
concat_nef()
