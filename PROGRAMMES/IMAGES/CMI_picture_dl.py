import os
import sys
import shutil
import config_image as CI
from lxml import etree
from ftplib import FTP


listing = []
liste_name = []
check_list = []
final_tuple = ()

def     check_folder(list_f):

    list_a_dl = []
    for elem in list_f:
        data = elem.split('/')
        name = data[len(data)-1]
        name_check = name[:-12]
        liste_name.append(name_check)
    src_list_file = os.listdir(CI.clean_path)
    for elem in src_list_file:
        data = elem[6:]
        check_list.append(data)
    diff_list = list(set(liste_name) - set(check_list))
    for elem in diff_list:
        for elem_2 in list_f:
            if elem in elem_2:
                list_a_dl.append(elem_2)
    print("{} {}".format("Nombres d'images a DL :", len(list_a_dl)))
    return(list_a_dl)


def     delimite(data):
                    
    sp = data.split('/')
    name = sp[len(sp)-1]
    l_name = (len(name))
    path = data[:-l_name]
    path_image = "/" + path
    final_tuple = (path_image, name)
    return final_tuple


def     list_image(source_file):

    with open(source_file) as data:
        for line in data:
            if "PhysicalAssetCropped" in line:
                value = line
                tt = value[30:-24]
                vv = tt.split("/")
                name = (vv[len(vv)-1])
                name_data = name[:-12]
                ll = (len(name))
                data = value[30:-24]
                path = "/" + data[:-ll]
                listing.append(data)
        
                
                
                
def     images_dl(list):

    nb_images_dl = len(list)
    with FTP(CI.adress_server) as ftp:
        ftp.login(CI.user, CI.password)
        for elem in list:
            try:
                path = delimite(elem)[0]
                name = delimite(elem)[1]
                ftp.cwd(path)
                fd = open(name, 'wb')
                ftp.retrbinary('RETR ' + name, fd.write)
                file_name = "CMI24_" + name[:-12]
                shutil.move(CI.src_cmi_xml_genese + name, CI.picture_day_trt + file_name)
            except:
                print("ERROR DL : " + elem)



            
def     listing_images(path):

    nb_xml = 0
    os.chdir(path)
    listing_file = os.listdir(path)
    print("DEBUT LISTING DES XML CMI24")
    for elem in listing_file:
        if ".xml" in elem :
            nb_xml += 1
            list_image(elem)
    print("FIN DU LISTING DES XML CMI24")
    print("{} {}".format("nombre de xml CMI24 :", nb_xml))
    print("{} {}".format("liste d'images initial de CMI24:", len(listing)))
    images_dl(check_folder(listing))
    







listing_images(CI.src_cmi_xml_genese)
