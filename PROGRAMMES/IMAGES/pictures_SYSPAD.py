import sys
import os
from lxml import etree
import config_image as CI
import shutil

listing = []

def     check_data_flag(source_file, balise):
    
    xml = etree.parse(source_file)
    root = xml.getroot()
    for elem in root.iter(balise):
        new_elem = elem.text


def     check_data_attrib(source_file, balise, attrib):

    xml = etree.parse(source_file)
    root = xml.getroot()
    for elem in root.iter(balise):
        new_elem = elem.get(attrib)
        listing.append(new_elem)
    

def     check_folder(list_clean):

    index = 0
    index_2 = 0
    list_name = []
    os.chdir(CI.src_sys)
    print("{} {}".format("images SYSPAD totales des xml sans doublons: ", len(list_clean)))
    for elem in list_clean:
        data = elem + ".jpg"
        list_name.append(data)
    source_list = os.listdir(CI.clean_path)
    diff_list = list(set(list_name) - set(source_list))
    print("{} {}".format("Nb en manque : ",len(diff_list)))
    for elem in diff_list:
        try:
            index += 1
            shutil.copy2(CI.src_sys + elem, CI.picture_day_trt + elem)
        except:
            index_2 += 1
#            print(elem + " DOESN'T EXIST")
    print("{} {}".format("nb images trouvees :",index))
    print("{} {}".format("nb images non trouvees :",index_2))
    print("{} {}".format("Nb en manque : ",len(diff_list)))


def     init(path):

    source_file = path
    os.chdir(path)
    listing_file = os.listdir(path)
    for elem in listing_file:
        if ".xml" in elem and "geo." not in elem:
            try:
                check_data_flag(elem, "FilePath")
                check_data_attrib(elem, "DigitalAsset", "StepId")
            except :
                print("ERROR : " + elem)
    l = list(set(listing))
    check_folder(l)



    
            
init(CI.src_sys_xml_genese)
