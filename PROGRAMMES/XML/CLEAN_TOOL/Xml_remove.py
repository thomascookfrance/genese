import os
import glob
import sys

class EraseXmlDuplicates:

    def __init__(self, origin):
        self.origin = origin
        self.tmp = origin + "/tmp"
        self.order = "0123456789ABCDEFGHIJKLMNOPQRSTWXYZ"
        os.mkdir(self.tmp)
        
    def __del__(self):
        os.rmdir(self.tmp)
        
    def move_file_to_tmp(self, idx):
        file_list = glob.glob(self.origin + "/" + self.order[idx] + "*.xml")
        for idx in range(0, len(file_list)):
            os.rename(file_list[idx], file_list[idx].replace(self.origin, self.tmp))
            
    def move_file_to_origin(self):
        file_list = glob.glob(self.tmp + "/*")
        for idx in range(0, len(file_list)):
            os.rename(file_list[idx], file_list[idx].replace(self.tmp, self.origin))
                        
    def get_list_tmp(self):
        xml_file_list = os.listdir(self.tmp)
        return [tuple(x.split(".fr.")) for x in xml_file_list] # create tuple(name, date) for every file in origin/tmp
                    
    def erase_duplicates(self):
        file_list = self.get_list_tmp()
        file_list.sort()
        for idx in range(len(file_list) - 1, 0, -1):
            if file_list[idx -1][0] == file_list[idx][0]:
                os.remove(self.tmp + "/" + file_list[idx -1][0] + ".fr." + file_list[idx - 1][1]) # remove file with anterior date
                del file_list[idx - 1]
                        
def main(origin):
    if origin[-1] == '/':
        origin = origin[:-1]
    if os.path.exists(origin):
        p = EraseXmlDuplicates(origin)
        for idx in range(0, len(p.order)):
            p.move_file_to_tmp(idx)
            p.erase_duplicates()
            p.move_file_to_origin()
        return 0
    else:
        print("Given path doesn't exist")
        return 1

    
if __name__ == "__main__":
        main("/home/transfert-content/SI_STRUCT_V2/SOURCES/XML/CMI24/")
        #main("/home/transfert-content/SI_STRUCT_V2/PROGRAMMES/XML/CMI24/")
       # main("/srv/data/SRC/CMI24/FTP_SYNC/")
        #main("/srv/data/SRC/CMI24/DATA/")
