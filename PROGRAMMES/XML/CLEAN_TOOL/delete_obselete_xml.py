import sys
import os
import codecs
import shutil

def     cleanner(path_to_clean):

    counter = 0
    source_file = path_to_clean
    destination_file = '/srv/SRC/SOURCES/XML/OBSELETE/'
    os.chdir(destination_file)
    list = os.listdir(destination_file)
    if len(list) > 0:
        for elem in list:
            os.remove(elem)
    os.chdir(source_file)
    file_list = os.listdir(source_file)
    for elem in file_list :
        if "geo." not in elem :
            if ".xml" in elem :
                name_file = elem
                f = codecs.open(name_file, 'r', 'utf-8')
                for it in f.readlines():
                    if "<Status>Obsolete" in it or "<Status></Status>" in it or "<Status>Print only" in it or "<Status>Staging" in it :
                        counter += 1
                        print("file : ", elem, "balise : ", it)
                        shutil.move(name_file, destination_file)
    print(counter)
                        

cleanner('/srv/SRC/SOURCES/XML/SYSPAD/')
