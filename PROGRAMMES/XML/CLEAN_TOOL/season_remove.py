import sys
import os
from lxml import etree

#source_dir = "/home/transfert-content/SI_STRUCT_V2/SOURCES/XML/CMI24/"
source_dir = "/home/transfert-content/SI_STRUCT_V2/PROGRAMMES/XML/CMI24/"
#source_dir = "/srv/data/FTP/content-orchestra2/orchestra-step2step/JET/STEP/STEP/STEP/"
dest_file = "/home/transfert-content/SI_STRUCT_V2/SOURCES/XML/OBSELETE/"




def     check_data_flag(source_file, balise):
    xml = etree.parse(source_file)
    root = xml.getroot()
    for elem in root.iter(balise):
        balise_data = elem.text
        return balise_data


def     remove_season(source_file, season):
    data = check_data_flag(source_file, 'Season')
    if data == season:
        print("{}---{}".format(source_file, season))
        os.remove(source_file)



def     init_clean(season):
    os.chdir(source_dir)
    listing_file = os.listdir(source_dir)
    for elem in listing_file:
        if ".xml" in elem :
            remove_season(elem, season)


init_clean(sys.argv[1])
