import os
import sys
import config_xml as CX
import shutil
from ftplib import FTP


def     xml_dl():

    os.chdir(CX.path_dest)
    list_file_d = os.listdir(CX.path_dest)
    for elem in list_file_d:
        if ".xml" in elem:
            shutil.move(CX.path_dest + elem, CX.path_bk + elem)
    src_files = os.listdir(CX.path_dest)
    with FTP(CX.adress_server) as ftp :
        ftp.login(CX.user,CX.password)
        ftp.cwd('TCF')
        list_file = ftp.nlst()
        for elem in list_file:
            if elem not in src_files :
                fl = open(elem, 'wb')
                ftp.retrbinary('RETR ' + elem, fl.write)
                shutil.move(elem, CX.path_dest + elem)
        
xml_dl()
