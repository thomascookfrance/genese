import os
import sys
import shutil
import config_xml as CX


def     init_listing(path):


    prog = 'xml_hotel_gen.py'
    os.chdir(CX.path_data)
    if os.path.isfile(prog):
        os.remove(prog)
    os.chdir(CX.path_script)
    shutil.copy2('xml_hotel_gen.py', path + 'xml_hotel_gen.py')
    os.chdir(path)
    listing_file = os.listdir(path)
    for elem in listing_file:
        if ".xml" in elem:
            print(elem)
            os.system("python3 xml_hotel_gen.py " + elem)

def     dispatch_files():
    
    os.chdir(CX.path_data)
    listing = os.listdir(CX.path_data)
    for elem in listing:
        if "JETN" in elem:
            shutil.move(CX.path_data + elem, CX.dest_path + elem)


print("Generation des XML CMI24")
init_listing(CX.path_data)
print("Fin de la generation des XML CMI24")
print("Envoie des XML V3 dans STEP_V3")
dispatch_files()
print("Fin d'envoie des XML en interne")
