import os
import sys
import requests
import json
from lxml import etree

country_file = "/srv/Software/PROG_GENESE/PROGRAMMES/XML/CONFIG/geo.269306.xml"


def     stocking_data(source_file, balise):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""
    
    for elem in root.iter(balise) :
        if elem is not None:
            new_elem = elem.text
        else :
            new_elem = ""
    return new_elem


def     stocking_value(source_file, balise, attrib):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""
    
    for elem in root.iter(balise):
        if elem is not None:
            new_elem = elem.get(attrib)
        else:
            new_elem = ""
    return new_elem
    

def     listing_topic(source_file, attribut, index):

    xml = etree.parse(source_file)
    erreur = "Pas de DATA"
    for elem in xml.xpath("/StepMessage/Product/Topics/Topic/Paragraph"):
        if (elem.get("Code")) == attribut:
            data = elem.getchildren()
            if data is not None:
                return(data[index].text)


def     code_image(source_file, index):
                    
    src = data_image_type(source_file, index)
    if src:
        tree = etree.parse(ref_images)
        root = tree.getroot()
        for code in root.findall('Code'):
            name = code.get('codeNumber')
            code_i = code.get('codeImage')
            if src in name:
                return code_i
    else:
        return None        


            
def     get_value(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Value"):
        if var == elem.get("AttributeID"):
            if elem.text :
                return (elem.text)
            else:
                return (None)
                                
        
def     attrib_data(source_file, code, index):

    xml = etree.parse(source_file)
    for it in xml.xpath("/StepMessage/Product/CommercialTexts/Slogan"):
        if (it.get("Code")) == code:
            data = it.getchildren()
            return data[index].text

def     longitude_data(source_file, balise):

    base = stocking_data(source_file, "Coordinate")
    if base is not None:
        l_1 = base.split('(')
        l_1b = l_1[1]
        l_2 = l_1b[:-1]
        longitude = l_2.split(' ')
        return longitude[0]


def     latitude_data(source_file, balise):

    base = stocking_data(source_file, "Coordinate")
    if base is not None :
        l_1 = base.split('(')
        l_1b = l_1[1]
        l_2 = l_1b[:-1]
        latitude = l_2.split(' ')
        return latitude[1]


def     iata_code(source_file):

    lattitude = latitude_data(source_file, "Coordinate")
    longitude = longitude_data(source_file, "Coordinate")
    latt = str(lattitude)
    lon = str(longitude)
    r = requests.get("http://iata.thomascook.fr:8080/iata/nearest?lat=" + latt + "&long=" + lon)
    if r:
        rr = json.loads(r.text)
        for cle, value in rr.items():
            if "code" in cle:
                iata = value
                return iata


def     nb_bedroom(source_file, var):

    nb = 0
    with open(source_file, 'r') as data:
        for line in data:
            if var in line:
                nb = nb + 1
    return nb


def     nb_images(source_file, var):

    nb = 0
    with open(source_file, 'r') as data:
        for line in data:
            if var in line:
                nb = nb + 1
    return nb


def     ratting(source_file):

    data = stocking_data(source_file, "OfficalRating")
    num = data.split(' ')
    return (num[0])



#----------------------------------------------------------------GEO


def     geo_file(balise):

    id = stocking_data(country_file, balise)
    if id:
        return id

#--------------------------------------------------------------

def             chambre_desc(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Chambres/Chambre"):
        data = elem.getchildren()
        listing.append(data[0].text)
        listing.append(data[1].text)
        listing.append(data[2].text)
        listing.append(data[3].text)
        listing.append(data[4].text)
        listing.append(data[5].text)
    return (listing[index])



def             list_image(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/StepMessage/Product/DigitalAssets/DigitalAsset"):
        data = elem.get("AssetName")
        listing.append(data)

    return(listing[index])


def     data_image(source_file, index):

    listing = []
    pref = "CMI24_"
    xml = etree.parse(source_file)
    for elem in xml.xpath("/StepMessage/Product/DigitalAssets/DigitalAsset"):
        data = elem.get("AssetName")
        name = pref + data[:-4]
        listing.append(name)
        listing.append(elem.get("IsMainImage"))

    return (listing[index])


def     data_image_type(source_file, index):

    listing = []
    pref = "CMI24_"
    xml = etree.parse(source_file)
    for elem in xml.xpath("/StepMessage/Product/DigitalAssets/DigitalAsset"):
        data = elem.get("AssetType")
        listing.append(data)
#        listing.append(elem.get("IsMainImage"))
    return (listing[index])


def     check_type(source_file):
    
    data = stocking_data(source_file, 'ObjectName')
    if "Texte Bed Bank" in data:
        type_file = "Bed_Bank"
    else:
        type_file = "TCI"
    return type_file


def     init_name(source_file):

    full_year = stocking_data(source_file, 'Season')
    year = full_year[1:]
    season = full_year[:1]
    ekms = stocking_data(source_file, 'ProductBookingsCode')
    name = "be.24." + year + "." + season + "." + ekms
    return name

    

def     bb_name(source_file):

    full_year = stocking_data(source_file, 'Season')
    year = full_year[1:]
    ekms_code = stocking_data(source_file, 'ProductBookingsCode')
    name = "be.24." + year + ".Y." + ekms_code
    return(name)
            

def     writting_XML(source_file):

    type_file = check_type(source_file)
    b_base = etree.Element('StepMessage')
    b_manifest = etree.SubElement(b_base,'Manifest')
    b_datasource = etree.SubElement(b_manifest, 'DataSource')
    b_datasource.text = stocking_data(source_file, 'DataSource')
    b_commercial = etree.SubElement(b_manifest, 'CommercialStepId')
    b_commercial.text = stocking_data(source_file, 'CommercialStepId')
    b_common = etree.SubElement(b_manifest, 'CommonStepId')
    b_common.text = stocking_data(source_file, 'CommonStepId')
    b_workspace = etree.SubElement(b_manifest, 'WorkspaceId')
    b_workspace.text = stocking_data(source_file, 'WorkspaceId')
    b_objectname = etree.SubElement(b_manifest, 'ObjectName')
    if type_file == 'Bed_Bank':
        b_objectname.text = bb_name(source_file)
    else:
        b_objectname.text = init_name(source_file)
    b_objecttype = etree.SubElement(b_manifest, 'ObjectType')
    b_objecttype.text = stocking_data(source_file, 'ObjectType')
    b_typeficheproduit = etree.SubElement(b_manifest, 'TypeFicheProduit')
    b_typeficheproduit.text = "STANDARD"
    b_typevoyage = etree.SubElement(b_manifest, 'TypeVoyage')
    b_value0 = etree.SubElement(b_typevoyage, 'Value')
    b_value0.text = stocking_data(source_file, 'StandardObjectType')    
    b_parentid = etree.SubElement(b_manifest, 'ParentId')
    b_parentid.text = stocking_data(source_file, "ParendId")
    b_revision = etree.SubElement(b_manifest, 'Revision')
    b_revision.text = stocking_data(source_file, 'Revision')
    b_langue = etree.SubElement(b_manifest, 'LanguageCode')
    b_langue.text = stocking_data(source_file, 'LanguageCode')
    b_path = etree.SubElement(b_manifest, 'FilePath')
    b_path.text = source_file
    b_name = etree.SubElement(b_manifest, 'FileName')
    b_name.text = "JETN.52." + b_commercial.text + ".xml"
    b_srcf = etree.SubElement(b_manifest, 'SourceFileName')
    b_srcf.text = "DATA/" + source_file
    b_asset = etree.SubElement(b_manifest, 'AssetPath')
    b_asset.text = "JET/IMAGES"

#---------------------------------------------------------------
    
    b_status = etree.SubElement(b_manifest, 'Status')
    b_status.text = "enabled"
        
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++PRODUCT
    b_productf = etree.SubElement(b_base, 'Product')
    b_back = etree.SubElement(b_productf, 'BackendCodes')    
    b_reserv = etree.SubElement(b_back, 'ReservationSystem')
    b_reserv.set('hotel', "")
    b_reserv.set('package', "")
    b_reserv.set('location', "")
    b_reserv.set('camping', "")
    b_reserv.text = stocking_data(source_file,"ReservationSystem")
    b_prodbook = etree.SubElement(b_back, 'ProductBookingsCode')
    b_prodbook.text = stocking_data(source_file, 'ProductBookingsCode')
    b_market = etree.SubElement(b_back, 'Market')
    b_market.text = stocking_data(source_file, "BrandCode")
    b_brandcode = etree.SubElement(b_back, 'BrandCode')
    b_brandcode.text = "JET"
#    b_brandcode.text = stocking_data(source_file, "CatalogueCode")
    b_ss = etree.SubElement(b_back, 'Season')
    b_ss.text = stocking_data(source_file, 'Season')
    b_channel = etree.SubElement(b_back, 'ChannelName')
    b_channel.text = "CMI24-JET"
    b_commer = etree.SubElement(b_productf, 'CommercialTexts')
    b_producti = etree.SubElement(b_commer, 'ProductTitle')
    b_producti.text = stocking_data(source_file, 'ProductTitle')
    b_slo = etree.SubElement(b_commer, 'Slogan')
    b_slo.text = stocking_data(source_file, "Slogan")
    b_introtext = etree.SubElement(b_commer, 'IntroText')
    b_introtext.text = stocking_data(source_file, "IntroText")
    b_topics = etree.SubElement(b_productf, 'Topics')
    b_topic = etree.SubElement(b_topics, 'Topic')

    b_topic.set('Code', 'FACILITIES')
    b_parag = etree.SubElement(b_topic, 'Paragraph')
    b_parag.set('Id', 'CHAMBRE')
    b_title = etree.SubElement(b_parag, 'Title')
    if 'Bed_Bank' in type_file:
        b_title.text = "Votre Hôtel"
        b_body = etree.SubElement(b_parag, 'Body')
        b_body.text = listing_topic(source_file, "BB_description_general", 1)
    else:
        b_body = etree.SubElement(b_parag, 'Body')
        b_body.text = listing_topic(source_file, "Situation", 1)
        b_title = etree.SubElement(b_parag, 'Title')
        b_title.text = "Votre Chambre"
        b_body = etree.SubElement(b_parag, 'Body')
        b_body.text = listing_topic(source_file, "Lodging", 1)


    if 'Bed_Bank' not in type_file:
        b_parag2 = etree.SubElement(b_topic, 'Paragraph')
        b_parag2.set('Id', 'RESTAURANT')
        if listing_topic(source_file, "Bars_Restaurants", 1) is not None:
            b_title = etree.SubElement(b_parag2, 'Title')
            b_title.text = "Les Restaurants"
            b_body = etree.SubElement(b_parag2, 'Body')
            b_body.text = listing_topic(source_file, "Bars_Restaurants", 1)
        if listing_topic(source_file, "Bars_Restaurants_Commercial", 0) is not None:
             b_body = etree.SubElement(b_parag2, 'Body')
             b_body.text = listing_topic(source_file, "Bars_Restaurants_Commercial", 0)
        b_parag3 = etree.SubElement(b_topic, 'Paragraph')
        b_parag3.set('Id', 'SERVICE')
        b_title = etree.SubElement(b_parag3, 'Title')
        b_title.text = "Les Services"
        if listing_topic(source_file, "Facilities", 1) is not None:
            b_body = etree.SubElement(b_parag3, 'Body')
            b_body.text = listing_topic(source_file, "Facilities", 1)
        if listing_topic(source_file, "Facilities_Commercial", 1) is not None :
            b_body = etree.SubElement(b_parag3, 'Body')
            b_body.text = listing_topic(source_file, "Facilities_Commercial", 1)
        b_parag4 = etree.SubElement(b_topic, 'Paragraph')
        b_parag4.set('title', "Votre Formule tout Compris")
        b_parag4.set('Id', 'LE_TOUT_COMPRIS')
        b_title = etree.SubElement(b_parag4, 'Title')
        b_body = etree.SubElement(b_parag4, 'Body')
        b_parag5 = etree.SubElement(b_topic, 'Paragraph')
        b_parag5.set('Id', 'AMBIANCE')
        if listing_topic(source_file, "Facilities", 1) is not None :
            b_body = etree.SubElement(b_parag5, 'Body')
            b_body.text = listing_topic(source_file, "Animation", 1)
        if listing_topic(source_file, "Sports_And_Leisure", 1) is not None:
            b_body = etree.SubElement(b_parag5, 'Body')
            b_body.text = listing_topic(source_file, "Sports_And_Leisure", 1)
        if listing_topic(source_file, "Sports_And_Leisure_Commercial", 1) is not None :
            b_body = etree.SubElement(b_parag5, 'Body')
            b_body.text = listing_topic(source_file, "Sports_And_Leisure_Commercial", 1)
        b_body = etree.SubElement(b_parag5, 'Body')
        b_body.text = ""#listing_topic(source_file, "")
        
        b_parag6 = etree.SubElement(b_topic, 'Paragraph')
        b_parag6.set('title', "L'esprit de famille")
        b_parag6.set('Id', 'SPORT')
        b_body = etree.SubElement(b_parag6, 'Title')
        b_body.text = "Enfant"
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = listing_topic(source_file, "Kids", 1)
        if listing_topic(source_file, "Children_Commercial", 1) is not None:
            b_body = etree.SubElement(b_parag6, 'Body')
            b_body.text = listing_topic(source_file, "Children_Commercial", 1)
        b_parag7 = etree.SubElement(b_topic, 'Paragraph')
        b_parag7.set('title', "Information Tarifaires")
        b_parag7.set('Id', 'FAMILLE')
        if listing_topic(source_file, "Arrival_Departure", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Arrive et Depart"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Arrival_Departure", 1)
        if listing_topic(source_file, "Particularities", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Particularites"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Particularities", 1)        
        if listing_topic(source_file, "Pets", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Animaux"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Pets", 1)
        
        if listing_topic(source_file, "Early_Booking_Discount", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Reservez tot"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Early_Booking_Discount", 1)
        
        if listing_topic(source_file, "Children_Discount", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Reduction Enfants"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Children_Discount", 1)
        
        if listing_topic(source_file, "Highlight_1_Enum", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Activite extra"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Highlight_1_Enum", 1)

        if listing_topic(source_file, "Additional_Price_Table_Info", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Information"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Additional_Price_Table_Info", 1)

        if listing_topic(source_file, "Free_Nights_Discount", 1) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Nuits Gratuites"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "Free_Nights_Discount", 1)

        if listing_topic(source_file, "General_Discount", 0) is not None:
            b_title = etree.SubElement(b_parag7, 'Title')
            b_title.text = "Reductions Supplementaires"
            b_body = etree.SubElement(b_parag7, 'Body')
            b_body.text = listing_topic(source_file, "General_Discount", 0)


    b_parag8 = etree.SubElement(b_topic, 'Paragraph')
    b_parag8.set('Id', 'NOS_PRIX_COMPRENNENT')
    b_title = etree.SubElement(b_parag8, 'Title')
    b_title.text = "Nos prix Comprennent"
    b_body = etree.SubElement(b_parag8, 'Body')
    b_body.text = "Dans le cadre d'un sejour avec transport :<br/> le transport, l'hebergemnt, les repas selon la formule choisie et les services comme mentionnes sur la presente offre. Dans le cadre d'un sejour en rendez-vous sur place : <br/> l'hebergement, les repas selon la formule choisie et les servies comme mentionnes sur la presente offre"
    if listing_topic(source_file, "Costs_Included", 1) is not None:
        b_body = etree.SubElement(b_parag8, 'Body')
        b_body.text = listing_topic(source_file, "Costs_Included", 1)
    b_parag9 = etree.SubElement(b_topic, 'Paragraph')
    b_parag9.set('Id', 'NOS_PRIX_NE_COMPRENNENT_PAS')
    b_title = etree.SubElement(b_parag9, 'Title')
    b_title.text = "Nos prix ne Comprennent pas"
    b_body = etree.SubElement(b_parag9, 'Body')
    b_body.text = "Dans le cadre d'un sejour avec ou sans transport :<br/> les transports alle/retour aeroport/lieu de sejour, l'assurance voyage, les depenses a caractere personnel, la taxe de sejour (sauf mention contraire)."
    if listing_topic(source_file, "To_Be_Paid_At_Hotel", 1) is not None:
        b_body = etree.SubElement(b_parag9, 'Body')
        b_body.text = listing_topic(source_file, "To_Be_Paid_At_Hotel", 1)
    if listing_topic(source_file, "To_Be_Paid_At_Reservation", 1) is not None:
        b_body = etree.SubElement(b_parag9, 'Body')
        b_body.text = listing_topic(source_file, "To_Be_Paid_At_Reservation", 1)
    

    b_cons = etree.SubElement(b_productf, 'ConceptHotel')
    b_cons.text = ""
    b_bien = etree.SubElement(b_productf, 'TitreBienvenue')
    b_bien.text = "BIENVENUE CHEZ VOUS"
    b_biens = etree.SubElement(b_productf, 'TitreAmbianceSport')
    b_biens.text = "ambiance et sport"
    b_geo = etree.SubElement(b_productf, 'GeoPolitical')
    b_loca = etree.SubElement(b_geo, 'Locality')
    b_loca.set('cityname', stocking_value(source_file, 'PlaceCode', 'label'))
    b_loca.set('countryCodeISO', stocking_value(source_file, 'CountryCode', 'label'))
    b_loca.set('referencial', 'IATA')
    b_loca.set('code', iata_code(source_file))
    b_coun = etree.SubElement(b_geo, 'CountryCode')
    b_coun.set('name', stocking_data(source_file, 'CountryCode'))
    b_coun.set('label', "")
    b_coun.set('ttlabel', '')
    b_rcod = etree.SubElement(b_geo, 'RegionCode')
    b_rcod.set('name', stocking_value(source_file, 'RegionCode', 'label'))
    b_rcod.set('label', stocking_value(source_file, 'RegionCode', 'label'))
    b_rcod.set('ttlabel', stocking_value(source_file, 'RegionCode', 'label'))
    b_pcod = etree.SubElement(b_geo, 'PlaceCode')
    b_pcod.set('name', stocking_value(source_file, 'PlaceCode', 'label'))
    b_pcod.set('label', stocking_value(source_file, 'PlaceCode', 'label'))
    b_pcod.set('ttlabel', stocking_value(source_file, 'PlaceCode', 'label'))
    b_pcod.set('nurvislabel','')
    b_pcod.set('airport','')
    b_gps = etree.SubElement(b_geo, 'GPS')
    b_gps.set('Longitude', longitude_data(source_file, "gps.geotag"))
    b_gps.set('Latitude',  latitude_data(source_file, "gps.geotag"))
    b_gps.text = stocking_data(source_file, "Coordinate")

    b_links = etree.SubElement(b_productf, "Links")
    b_link = etree.SubElement(b_links, "Link")
    b_link.set("Type", "FicheBienvenue")
    b_bien = etree.SubElement(b_link, 'ObjectName')
    b_bien.text = geo_file("ObjectName")
    b_bien = etree.SubElement(b_link, 'StandardObjectType')
    b_bien.text = geo_file("StandardObjectType")
    b_bien = etree.SubElement(b_link, 'CommercialStepId')
    b_bien.text = geo_file("CommercialStepId")
    b_bien = etree.SubElement(b_link, 'ParentId')
    b_bien.text = geo_file("ParentId")
    b_bien = etree.SubElement(b_link, 'Country')
    b_bien.text = get_value(country_file, "country.name")
    b_bien = etree.SubElement(b_link, 'Intro')
    b_bien.text = get_value(country_file, "be.geo.country.travelInfo")
    b_bien = etree.SubElement(b_link, 'Formalities')
    b_bien.text = get_value(country_file, "be.geo.country.travelFormalities")
    b_bien = etree.SubElement(b_link, 'Health')
    b_bien.text = get_value(country_file, "be.geo.country.HealthAndSafety") + get_value(country_file, "be.geo.country.vaccinsAndHealth")


    b_rati = etree.SubElement(b_productf, 'PointsOfReference')
    b_offi = etree.SubElement(b_rati, 'Club')
    b_offi = etree.SubElement(b_rati, 'Label')
    b_offi = etree.SubElement(b_rati, 'Logo')
    b_offi = etree.SubElement(b_rati, 'Wellness-type')
    b_offi = etree.SubElement(b_rati, 'Client-Award')
    b_offi = etree.SubElement(b_rati, 'Novelty')
    b_offi = etree.SubElement(b_rati, 'ProductType')
    b_offi.text = 'Sejour'
    b_offi = etree.SubElement(b_rati, 'Formula')
    b_offi.text = 'Voyage individuel'
    b_assets = etree.SubElement(b_productf, 'DigitalAssets')
    nb_asset = nb_images(source_file, "AssetType")
    data_nb = 0
    index = 0
    while nb_asset > 0 :
        b_asset = etree.SubElement(b_assets, 'DigitalAsset')
        b_asset.set('StepId', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('IsMainImage', data_image(source_file, data_nb))
        image = data_image(source_file, data_nb)
        if "false" in image:
            hot_im = "hotel"
        else:
            hot_im = "Primary Image"
        data_nb += 1
        b_asset.set('AssetType', hot_im)
        b_asset.set('AssetName', list_image(source_file, index))
        index += 1
        b_asset.set('MimeType', "image/jpeg")
        b_asset.set('StandardAssetType', "IMAGE")
        b_asset.set('Revision', "")
        b_alt = etree.SubElement(b_asset, 'AltText')
        b_cap = etree.SubElement(b_asset, 'Caption')
        nb_asset = nb_asset - 1

    b_catalog = etree.SubElement(b_productf, "RefInitial")
    b_catalog.text = stocking_data(source_file, "CatalogueCode")
    b_rati = etree.SubElement(b_productf, 'Ratings')
    b_offi = etree.SubElement(b_rati, 'OfficialRating')
    b_offi.text = stocking_data(source_file, "OfficalRating")
    b_offi = etree.SubElement(b_rati, 'TCFRating')
    b_offi.text = stocking_data(source_file, "OwnRating")
    b_offi = etree.SubElement(b_rati, 'WebRating')
    rat = stocking_data(source_file, "OfficalRating")
    rr = rat.split(' ')
    if len(rr[0]) == 1:
        b_offi.text = rr[0] + ".0"
    if len(rr[0]) > 1:
        b_offi.text = rr[0] 
        
    b_offer = etree.SubElement(b_productf, "Offers")
    
    b_spe = etree.SubElement(b_offer, "Special")
    b_off = etree.SubElement(b_spe, "ExclusiveOffers")
    b_off.text = ""
    b_speoff = etree.SubElement(b_spe, "SpecialOffersText")
    b_speoff.text = ""
    b_excluoff = etree.SubElement(b_spe, "ExclusiveOffersNote")
    b_excluoff.text = ""
    b_wedding = etree.SubElement(b_offer, "Wedding")
    b_wedding.text = ""
    
    b_prict = etree.SubElement(b_offer, "PriceFrom")
    b_dan = etree.SubElement(b_prict, "DaysAndNights")
    b_dan.text = ""
    b_price = etree.SubElement(b_prict, "Price")
    b_price.text = ""
    b_numprice = etree.SubElement(b_prict, "NumericalPrice")
    b_numprice.text = ""
    b_board = etree.SubElement(b_prict, "Board")
    b_board.text= ""
    b_note = etree.SubElement(b_prict, "Note")
    b_note.text= ""

    b_dep = etree.SubElement(b_prict, "Departure")

    b_lsea = etree.SubElement(b_offer, "LowSeason")
    b_lsprice = etree.SubElement(b_lsea, "Price")
    b_lnumprice = etree.SubElement(b_lsea, "NumericalPrice")
    b_ltitle = etree.SubElement(b_lsea, "Title")

    b_hsea = etree.SubElement(b_offer, "LowSeason")
    b_hsprice = etree.SubElement(b_hsea, "Price")
    b_hnumprice = etree.SubElement(b_hsea, "NumericalPrice")
    b_htitle = etree.SubElement(b_hsea, "Title")
    b_htitle.text = ""
    
    try:
        if type_file == "TCI":
            name_file = "JETN." + type_file + "." + stocking_data(source_file, 'Season') + "." + stocking_data(source_file, 'ProductBookingsCode') + ".xml"
        else :
            nb_season = stocking_data(source_file, 'Season')
            name_file = "JETN." + type_file + "." + nb_season[1:] + "." + stocking_data(source_file, 'ProductBookingsCode') + ".xml"
#        print("{}, {}".format(source_file, name_file))
        with open(name_file,'w') as fichier:
            fichier.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            fichier.write(etree.tostring(b_base,pretty_print=True).decode('utf-8'))
        
    except IOError:
        print('Problème rencontré lors de l\'écriture ...')
        exit(1)

writting_XML(sys.argv[1])
