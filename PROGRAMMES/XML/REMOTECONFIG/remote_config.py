import os
import sys
import shutil
from lxml import etree
import config_sep as CS



def     listing_file():

    os.chdir(CS.tci_dir)
    listing = os.listdir(CS.tci_dir)
    list_code = []
    for elem in listing:
        if ".TCI." in elem:
            name_file = elem
            sep = elem.split('.')
            list_code.append(sep[3])
    return(list_code)





def     remote():

    listing = listing_file()
    list_t = list(set(listing))
    print(len(list_t))
    b_base = etree.Element('filterConfiguration')
    b_valid = etree.SubElement(b_base,'SWvalidProducts')
    for elem in list_t:
        b_product = etree.SubElement(b_valid, 'product')
        b_product.text = elem
    try:
        with open("remoteConfigurationV2.xml",'w') as fichier:
            fichier.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            fichier.write(etree.tostring(b_base,pretty_print=True).decode('utf-8'))
    except IOError:
        print("fail")
        exit(1)



def     move_file():
    
    os.chdir(CS.tci_dir)
    shutil.move(CS.tci_dir + "remoteConfigurationV2.xml", CS.path_script + "remoteConfigurationV2.xml")
        
remote()
move_file()
