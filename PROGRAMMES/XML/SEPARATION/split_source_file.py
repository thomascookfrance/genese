import os
import sys
import shutil
import config_sep as CS
from lxml import etree


def             stock_data(source_file, balise):

    xml = etree.parse(source_file)
    root = xml.getroot()

    for elem in root.iter(balise):
        new_elem = elem.text
        return new_elem


    
def             split_file(path):

    tci_list = []
    jet_list = []

    os.chdir(CS.source_dir)
    os.chdir(path)
    listing = os.listdir(path)
    for elem in listing:
        if "TCI" in elem or "Bed_Bank" in elem:
            print("TCI ou Bed Bank file : " + elem)
            data = stock_data(elem, "RefInitial")
            if "NJLC" in data or "NEFY" in data or "NUSA" in data:
                jet_list.append(elem)
            else:
                tci_list.append(elem)
        else:
            print("SYSPAD file : " + elem)
            jet_list.append(elem)
    print("{} {}".format("fiche JET: ", len(jet_list)))
    print("{} {}".format("fiche TCI: ", len(tci_list)))
    for elem_jet in jet_list:
        shutil.copy2(CS.source_dir + elem_jet, CS.jet_dir + elem_jet)
    for elem_tci in tci_list:
        shutil.copy2(CS.source_dir + elem_tci, CS.tci_dir + elem_tci)
    


def     send_xml():
    
    os.chdir(CS.source_dir)
    listing = os.listdir(CS.source_dir)
    list_dest = [CS.giata,CS.monitoring,CS.onparou,CS.qcnscruise,CS.speedmedia,CS.vigimilia,CS.b2b_tt]
    for elem in listing:
        if ".xml" in elem:
            for dest in list_dest:
                shutil.copy2(CS.source_dir + elem, dest + elem)



print("split")
#split_file(CS.source_dir)
print("send")
send_xml()
