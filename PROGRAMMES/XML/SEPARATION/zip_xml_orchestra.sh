#!/bin/bash


cd "/srv/SRC/SOURCES/XML/TCI_FILES/"
#rm test_v3TCI.zip
rm TCI.zip
zip TCI.zip *.xml
#zip test_v3TCI.zip *.xml

#------------------------------------------------------------prod
cp TCI.zip "/srv/data/FTP/content-orchestra2/XML/TCI_XML_ZIP/"
cp TCI.zip "/srv/data/FTP/content-orchestra2/orchestra-step2step/JET/STEP/STEP/STEP/TCI_XML_ZIP/"

#------------------------------------------------------------recette
#cp TCI.zip "/srv/data/FTP/content-orchestra2/FICHE_V3"
#cp test_v3TCI.zip "/srv/data/FTP/content-orchestra2/FICHE_V3"



cd "/srv/SRC/SOURCES/XML/JET_TOUR_FILES/"
rm JET_TOUR.zip
#rm test_v3.zip
zip JET_TOUR.zip *.xml
#zip test_v3.zip *.xml

#------------------------------------------------------------prod
cp JET_TOUR.zip "/srv/data/FTP/content-orchestra2/XML/JET_TOUR_XML_ZIP"
cp JET_TOUR.zip "/srv/data/FTP/content-orchestra2/orchestra-step2step/JET/STEP/STEP/STEP/JET_TOUR_XML_ZIP/"

#------------------------------------------------------------recette
#cp JET_TOUR.zip "/srv/data/FTP/content-orchestra2/FICHE_V3"
#cp test_v3.zip "/srv/data/FTP/content-orchestra2/FICHE_V3"

