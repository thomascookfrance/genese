import os
import sys
import shutil
import config_xml as CX


def     init_listing(path):

    prog_1 = "xml_hot_gen.py"
    prog_2 = "xml_roundtrip_gen.py"
    os.chdir(CX.path_data)
    if  os.path.isfile(prog_1):
        os.remove(prog_1)
    if  os.path.isfile(prog_2):
        os.remove(prog_2)
    os.chdir(CX.path_script)
    shutil.copy2('xml_hot_gen.py', CX.path_data + 'xml_hot_gen.py')
    shutil.copy2('xml_roundtrip_gen.py', CX.path_data +'xml_roundtrip_gen.py')
    os.chdir(path)
    listing_file = os.listdir(path)
    for elem in listing_file:
        if "hotel.comm" in elem :
            os.system("python3 xml_hot_gen.py " + (elem))
        if "roundtrip.comm" in elem :
            os.system("python3 xml_roundtrip_gen.py " + (elem))


def     dispatch_file():

    os.chdir(CX.path_data)
    listing = os.listdir(CX.path_data)
    for elem in listing:
        if "JETN" in elem:
            shutil.move(CX.path_data + elem, CX.dest_path + elem)



print("Generation des XML SYSPAD")            
init_listing(CX.path_data)
print("Fin de la generation des XML SYSPAD")
print("Envoie des XML SYSPAD")
dispatch_file()
print("Fin d'envoie des XML en interne")
