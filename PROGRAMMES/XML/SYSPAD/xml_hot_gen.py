import os
import sys
import requests
import json
from lxml import etree


country_file = "/srv/Software/PROG_GENESE/PROGRAMMES/XML/CONFIG/Countries.xml"
ref_images = "/srv/Software/PROG_GENESE/PROGRAMMES/XML/CONFIG/Code_images.xml"

def     stocking_data(source_file, balise):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""
    
    for elem in root.iter(balise) :
        if elem is not None:
            new_elem = elem.text
        else :
            new_elem = "X"
    return new_elem


def     stocking_value(source_file, balise, attrib):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""
    
    for elem in root.iter(balise):
        if elem is not None:
            new_value = elem.get(attrib)
        else:
            new_elem = "X"
    return new_value
    

def     init_name(source_file, code, type_p):

    list = source_file.split('.')
    name = 'JETN.'+ type_p +'.'+ code +'.'+ list[3]
    return(name)

def     attrib_data(source_file, code, index):

    xml = etree.parse(source_file)
    for it in xml.xpath("/StepMessage/Product/Topics/Topic/Paragraph"):
        if (it.get("Code")) == code:
            data = it.getchildren()
            return data[index].text

        
def     get_value(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Value"):
        if var == elem.get("AttributeID"):
            if elem.text :
                return (elem.text)
            else:
                return (None)
            


def     get_value_2(source_file, var, index):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Multivalue"):
        if (elem.get("AttributeID")) == var:
            data = elem.getchildren()
            return (data[index].text)


        
def     get_nb_value(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Multivalue"):
        if (elem.get("AttributeID")) == var:
            data = len(elem.getchildren())
            return(data)

        
def     longitude_data(source_file, balise):

    base = get_value(source_file, balise)
    if base:
        if len(base) > 8 :
            l_1 = base.split('(')
            l_1b = l_1[1]
            l_2 = l_1b[:-1]
            longitude = l_2.split(' ')
            return longitude[0]
        else:
            return(base)


def     latitude_data(source_file, balise):

    base = get_value(source_file, balise)
    if base:
        if len(base) > 8:
            l_1 = base.split('(')
            l_1b = l_1[1]
            l_2 = l_1b[:-1]
            latitude = l_2.split(' ')
            return latitude[1]
        else:
            return(base)


def     code_iso(source_file):

    src = get_value(source_file, "country.name")
    if src:
        data = src.upper()
        tree = etree.parse(country_file)
        root = tree.getroot()
        for country in root.findall('Country'):
            name = country.get('countryName')
            code = country.get('codeISO')
            if data in name:
                return code
    else:
         return(None)       



def     code_image(src):

    tree = etree.parse(ref_images)
    root = tree.getroot()
    for elem in root.findall('Code'):
        name = elem.get('codeNumber')
        code_i = elem.get('codeImage')
        if src in name:
            return(code_i)

     
def     code_iso_name(source_file):

    src = get_value(source_file, "country.name")
    if src:
        data = src.upper()
        return (data)

                                            

def     iata_code(source_file):

    lattitude = latitude_data(source_file, "gps.geotag")
    longitude = longitude_data(source_file, "gps.geotag")
    latt = str(lattitude)
    lon = str(longitude)
    r = requests.get("http://iata.thomascook.fr:8080/iata/nearest?lat=" + latt + "&long=" + lon)
    if r:
        rr = json.loads(r.text)
        for cle, value in rr.items():
            if "code" in cle:
                iata = value
                return iata
    

def     get_bedroom_desc(source_file, balise):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Value"):
        if balise == elem.get("AttributeID"):
            if elem.text:
                text = elem.text.split('<b>')
                return(text[0])


def     nb_bedroom(source_file, var):

    nb = 0
    with open(source_file, 'r') as data:
        for line in data:
            if var in line:
                nb = nb + 1
    return nb


def     nb_images(source_file, var):

    nb = 0
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Assets/DigitalAsset"):
        nb += 1
    return nb


def     get_value_3(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Multivalue"):
        if (elem.get("AttributeID")) == var:
            data = len(elem.getchildren())
            return (data)

#----------------------------------------------------------------GEO


def     geo_file(source_file):

    id = get_value(source_file, "fr.prod.hotel.destidescriptionref")
    if id:
        filename = id + ".xml"
        return filename

#--------------------------------------------------------------

def             chambre_desc(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Chambres/Chambre"):
        data = elem.getchildren()
        listing.append(data[0].text)
        listing.append(data[1].text)
        listing.append(data[2].text)
        listing.append(data[3].text)
        listing.append(data[4].text)
        listing.append(data[5].text)
    return (listing[index])


def     data_image(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Assets/DigitalAsset"):
        listing.append(elem.get("StepId"))
        listing.append(elem.get("IsMainImage"))
        listing.append(elem.get("AssetType"))
        listing.append(elem.get("AssetName"))
        listing.append(elem.get("width"))
        listing.append(elem.get("height"))
    return (listing[index])


def     contrat_letter(source_file):

    list = []
    list_contrat = []
    ekms_code = get_value(source_file, "backend.productCode")
    nb_value = get_nb_value(source_file, "backend.subProductCodes")
    if nb_value :
        index = 0
        while index < nb_value :
            data = get_value_2(source_file, "backend.subProductCodes", index)
            list.append(data)
            index += 1
        for elem in list:
            cc = ekms_code + elem
            list_contrat.append(cc)
        return(list_contrat)









    
def     writting_XML(source_file):

    b_base = etree.Element('StepMessage')
    b_manifest = etree.SubElement(b_base,'Manifest')
    b_datasource = etree.SubElement(b_manifest, 'DataSource')
    b_datasource.text = stocking_data(source_file, 'DataSource')
    b_commercial = etree.SubElement(b_manifest, 'CommercialStepId')
    b_commercial.text = stocking_data(source_file, 'CommercialStepId')
    b_common = etree.SubElement(b_manifest, 'CommonStepId')
    b_common.text = stocking_data(source_file, 'CommonStepId')
    b_workspace = etree.SubElement(b_manifest, 'WorkspaceId')
    b_workspace.text = stocking_data(source_file, 'WorkspaceId')
    b_objectname = etree.SubElement(b_manifest, 'ObjectName')
    b_objectname.text = stocking_data(source_file, 'ObjectName')
    b_objecttype = etree.SubElement(b_manifest, 'ObjectType')
    b_objecttype.text = stocking_data(source_file, 'ObjectType')

    check_type = get_value(source_file, "fr.prod.hotel.productType")
    
    b_typeficheproduit = etree.SubElement(b_manifest, 'TypeFicheProduit')
    if check_type :
        if "Club" in check_type:
            b_typeficheproduit.text = "CLUB"
        else :
            b_typeficheproduit.text = "STANDARD"
    b_typevoyage = etree.SubElement(b_manifest, 'TypeVoyage')
    b_value0 = etree.SubElement(b_typevoyage, 'Value')
    if check_type :
        if "Club" in check_type:
            b_value0.text = "CLUB"    
        else:
            b_value0.text = "SEJOUR"
    b_value1 = etree.SubElement(b_typevoyage, 'Value')
    b_value1.text = "HOTEL"    
    b_parentid = etree.SubElement(b_manifest, 'ParentId')
    b_parentid.text = b_common.text
    b_revision = etree.SubElement(b_manifest, 'Revision')
    b_revision.text = stocking_data(source_file, 'Revision')
    b_langue = etree.SubElement(b_manifest, 'LanguageCode')
    b_langue.text = stocking_data(source_file, 'LanguageCode')
    b_path = etree.SubElement(b_manifest, 'FilePath')
    b_path.text = source_file
    b_name = etree.SubElement(b_manifest, 'FileName')
    b_name.text = "JETN." + source_file
    b_srcf = etree.SubElement(b_manifest, 'SourceFileName')
    b_srcf.text = source_file
    b_asset = etree.SubElement(b_manifest, 'AssetPath')
    b_asset.text = "JET/IMAGES"
    b_status = etree.SubElement(b_manifest, 'Status')
    if "Active" in stocking_data(source_file, "Status"):
        b_status.text = "enabled"
    else:
        b_status.text = "Obselete"
        
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++PRODUCT
    b_productf = etree.SubElement(b_base, 'Product')
    b_back = etree.SubElement(b_productf, 'BackendCodes')    
    b_reserv = etree.SubElement(b_back, 'ReservationSystem')
    b_reserv.set('hotel', "")
    b_reserv.set('package', "")
    b_reserv.set('location', "")
    b_reserv.set('camping', "")
    b_reserv.text = get_value(source_file,"backend.backendSystem")
    b_prodbook = etree.SubElement(b_back, 'ProductBookingsCode')
    data = contrat_letter(source_file)
    if data:
        str_1 = ';'.join(str(x) for x in data)
        b_prodbook.text = str_1
    b_market = etree.SubElement(b_back, 'Market')
    b_market.text = "TC-FR"
    b_brandcode = etree.SubElement(b_back, 'BrandCode')
    b_brandcode.text = "JET"
    b_channel = etree.SubElement(b_back, 'ChannelName')
    b_channel.text = "Compos Juliot Jet tours"
    b_commer = etree.SubElement(b_productf, 'CommercialTexts')
    b_producti = etree.SubElement(b_commer, 'ProductTitle')
    b_producti.text = stocking_data(source_file, 'ProductTitle')
    b_producti.text = get_value(source_file, "hotel.name")
    b_slo = etree.SubElement(b_commer, 'Slogan')
    b_introtext = etree.SubElement(b_commer, 'IntroText')
    b_introtext.text = get_value(source_file, "fr.prod.hotel.introText")
    b_topics = etree.SubElement(b_productf, 'Topics')
    b_topic = etree.SubElement(b_topics, 'Topic')
    b_topic.set('Code', 'FACILITIES')
    b_parag = etree.SubElement(b_topic, 'Paragraph')
    b_parag.set('Id', 'CHAMBRE')
    b_title = etree.SubElement(b_parag, 'Title')
    b_title.text = get_value(source_file, "fr.prod.hotel.lodging.title")
    b_body = etree.SubElement(b_parag, 'Body')
    b_body.text = get_bedroom_desc(source_file, "fr.prod.hotel.lodging")
    b_parag2 = etree.SubElement(b_topic, 'Paragraph')

    b_parag2.set('Id', 'RESTAURANT')
    b_title = etree.SubElement(b_parag2, 'Title')
    b_title.text = get_value(source_file, "fr.prod.hotel.mealsAndDrinks.title")
    b_body = etree.SubElement(b_parag2, 'Body')
    b_body.text = get_value(source_file, "fr.prod.hotel.mealsAndDrinks")
    b_parag3 = etree.SubElement(b_topic, 'Paragraph')

    b_parag3.set('Id', 'SERVICE')
    b_title = etree.SubElement(b_parag3, 'Title')
    b_title.text = get_value(source_file, "fr.prod.hotel.atYourService.title")
    b_body = etree.SubElement(b_parag3, 'Body')
    b_body.text = get_value(source_file, "fr.prod.hotel.atYourService")

    b_parag4 = etree.SubElement(b_topic, 'Paragraph')
    r = get_value(source_file, "fr.prod.hotel.allInc.type")
    if r :
        b_parag4.set('title', get_value(source_file, "fr.prod.hotel.allInc.type"))
        b_parag4.set('Id', 'LE_TOUT_COMPRIS')
        b_title = etree.SubElement(b_parag4, 'Title')
        b_title.text = get_value(source_file, "fr.prod.hotel.allInc.table.title")
        b_body = etree.SubElement(b_parag4, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.allInc.table")
        b_title = etree.SubElement(b_parag4, 'Title')
        b_title.text = get_value(source_file, "fr.prod.hotel.allInc.bar.title")
        b_body = etree.SubElement(b_parag4, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.allInc.bar")

    b_parag5 = etree.SubElement(b_topic, 'Paragraph')
    b_parag5.set('Id', 'AMBIANCE')
    if check_type == 'Club':
        b_title = etree.SubElement(b_parag5, 'Title')
        b_title.text = get_value(source_file, "fr.prod.ambiance1.title")
        b_body = etree.SubElement(b_parag5, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.ambiance1")
    else:    
        b_title = etree.SubElement(b_parag5, 'Title')
        b_title.text = get_value(source_file, "fr.prod.hotel.activitiesAndLeisure.title")
        b_body = etree.SubElement(b_parag5, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.activitiesAndLeisure")
        b_title = etree.SubElement(b_parag5, 'Title')
        b_title.text = get_value(source_file, "fr.prod.hotel.wellness.title")
        b_body = etree.SubElement(b_parag5, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.wellness")

    

    b_parag6 = etree.SubElement(b_topic, 'Paragraph')
    b_parag6.set('title', "")
    b_parag6.set('Id', 'SPORT')
    if check_type == 'Club':
        b_title = etree.SubElement(b_parag6, 'Title')
        b_title.text = get_value(source_file, "fr.prod.hotel.ambiance2.title")
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = get_value(source_file, "fr.prod.ambiance2")
        b_body = etree.SubElement(b_parag6, 'Title')
        b_body.text = get_value(source_file, "fr.prod.hotel.SportsAndLeisure.title")
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = get_value(source_file, "attr.1257")
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = get_value(source_file, "attr.1337")
        b_titre = etree.SubElement(b_parag6, 'Title')
        b_titre.text = get_value(source_file, "fr.prod.hotel.golf.title")
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.golf")

    else:
        b_body = etree.SubElement(b_parag6, 'Title')
        b_body.text = get_value(source_file, "fr.prod.hotel.SportsAndLeisure.title")
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = get_value(source_file, "attr.1257")
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = get_value(source_file, "attr.1337")
        b_titre = etree.SubElement(b_parag6, 'Title')
        b_titre.text = get_value(source_file, "fr.prod.hotel.golf.title")
        b_body = etree.SubElement(b_parag6, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.golf")
        b_tt_p = etree.SubElement(b_parag6, 'Title')
        b_tt_p.text = get_value(source_file, "fr.prod.hotel.diving.title")
        b_body_p = etree.SubElement(b_parag6, 'Body')
        b_body_p.text = get_value(source_file, "fr.prod.hotel.diving")
    


    
    b_parag7 = etree.SubElement(b_topic, 'Paragraph')
    b_parag7.set('title', "POUR VOS ENFANTS")
    b_parag7.set('Id', 'FAMILLE')
    b_body = etree.SubElement(b_parag7, 'Body')
    b_body.text = get_value(source_file, "fr.prod.hotel.welcomeForFamilies")
    b_title = etree.SubElement(b_parag7, 'Title')
    b_title.text = get_value(source_file, "fr.prod.hotel.children.title")
    b_body = etree.SubElement(b_parag7, 'Body')
    b_body.text = get_value(source_file, "fr.prod.hotel.children")
    if check_type == "Club" :
        b_title = etree.SubElement(b_parag7, 'Title')
        b_title.text = get_value(source_file, "fr.prod.hotel.welcomeForKids.title")
        b_body = etree.SubElement(b_parag7, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.welcomeForKids")
        b_title = etree.SubElement(b_parag7, 'Title')
        b_title.text = get_value(source_file, "fr.prod.hotel.welcomeForAdos.title")
        b_body = etree.SubElement(b_parag7, 'Body')
        b_body.text = get_value(source_file, "fr.prod.hotel.welcomeForAdos")



    
    b_parag8 = etree.SubElement(b_topic, 'Paragraph')
    b_parag8.set('Id', 'NOS_PRIX_COMPRENNENT')
    b_title = etree.SubElement(b_parag8, 'Title')
    b_title.text = get_value(source_file, "fr.prod.hotel.inclInPriceLegal.title")
    b_body = etree.SubElement(b_parag8, 'Body')
    b_body.text = get_value(source_file, "fr.prod.hotel.inclInPriceLegal")
    
    b_parag9 = etree.SubElement(b_topic, 'Paragraph')
    b_parag9.set('Id', 'NOS_PRIX_NE_COMPRENNENT_PAS')
    b_title = etree.SubElement(b_parag9, 'Title')
    b_title.text = get_value(source_file, "fr.prod.hotel.notInclInPriceLegal.title")
    b_body = etree.SubElement(b_parag9, 'Body')
    b_body.text = get_value(source_file, "fr.prod.hotel.notInclInPriceLegal")
    
#--------------------------------------------------------------------------------
    b_parag10 = etree.SubElement(b_topic, 'Paragraph')
    b_parag10.set('Id', 'HEBERGEMENT')
    b_body = etree.SubElement(b_parag10, 'Body')
    b_title = etree.SubElement(b_parag10, 'Title')
    
    b_parag11 = etree.SubElement(b_topic, 'Paragraph')
    b_parag11.set('Id', 'VOYAGEZ MALIN')
    b_body = etree.SubElement(b_parag11, 'Body')
    b_title = etree.SubElement(b_parag11, 'Title')

    b_parag12 = etree.SubElement(b_topic, 'Paragraph')
    b_parag12.set('Id', 'VOUS ALLEZ AIMER')
    b_body = etree.SubElement(b_parag12, 'Body')
    b_body.text = get_value(source_file, "fr.prod.hotel.highlights")
    
    b_parag13 = etree.SubElement(b_topic, 'Paragraph')
    b_parag13.set('Id', 'INFOS VOYAGE')
    b_body = etree.SubElement(b_parag13, 'Body')
    b_title = etree.SubElement(b_parag13, 'Title')

    b_parag14 = etree.SubElement(b_topic, 'Paragraph')
    b_parag14.set('Id', 'PRINCIPAUX SITES')
    b_body = etree.SubElement(b_parag14, 'Body')
    b_title = etree.SubElement(b_parag14, 'Title')

    b_parag15 = etree.SubElement(b_topic, 'Paragraph')
    b_parag15.set('Id', 'PARTEZ BIEN INFORMES')
    b_body = etree.SubElement(b_parag15, 'Body')
    b_title = etree.SubElement(b_parag15, 'Title')
#--------------------------------------------------------------------------------
    b_tobloc = etree.SubElement(b_topics, 'Topic')
    b_tobloc.set('code', 'BLOCKS')
    nb_days = 0
    i = 0
    while i < nb_days :
        b_paracir = etree.SubElement(b_tobloc, 'Paragraph')
        b_paracir.set('Code', '')
        b_paracir.set('DigitalAsset', '')
        b_paracir.set('Id', '')
        b_paracir.set('Day', '')
        b_body = etree.SubElement(b_paracir, 'Body')
        i = i + 1

    b_dep = etree.SubElement(b_productf, 'DepartGarantis')
    b_annul = etree.SubElement(b_productf, 'PolitiqueAnnulation')
    b_picdep = etree.SubElement(b_productf,'PictoDepartGarantis')
    b_ptc = etree.SubElement(b_productf,'PictoTropheClient')
    b_pcp = etree.SubElement(b_productf,'PictoCircuitPrivatisable')
    b_pgp = etree.SubElement(b_productf,'PictoGuideParis')
    b_tb = etree.SubElement(b_productf,'TitreBarometre')
    b_vc = etree.SubElement(b_productf,'VideoCircuit')
    b_ap = etree.SubElement(b_productf,'AvisProfessionel')
    b_np = etree.SubElement(b_productf,'NomProfessionel')
    b_hc = etree.SubElement(b_productf,'HebergementContractuel')
    b_oe = etree.SubElement(b_productf,'OffreExtension')
    b_tpext = etree.SubElement(b_oe,'TitrePrincipalExtension')
    b_ofre1 = etree.SubElement(b_oe,'Offre1')
    b_jne = etree.SubElement(b_ofre1,'JoursNuitsExtension1')
    b_dte = etree.SubElement(b_ofre1,'DestinationExtension1')
    b_de = etree.SubElement(b_ofre1,'DescriptifExtension1')
    b_ofre2 = etree.SubElement(b_oe,'Offre2')
    b_jne2 = etree.SubElement(b_ofre2,'JoursNuitsExtension1')
    b_dte2 = etree.SubElement(b_ofre2,'DestinationExtension1')
    b_de2 = etree.SubElement(b_ofre2,'DescriptifExtension1')

#--------------------------------------------------------------------------------

    b_cons = etree.SubElement(b_productf, 'ConceptHotel')
    b_cons.text = get_value_2(source_file, "fr.prod.hotel.label", 0)
    b_bien = etree.SubElement(b_productf, 'TitreBienvenue')
    b_bien.text = "BIENVENUE CHEZ VOUS"
    b_biens = etree.SubElement(b_productf, 'TitreAmbianceSport')
    b_biens.text = "ambiance et sport"
    b_inft = etree.SubElement(b_productf, 'InfoTransfert')
    b_inft.text = "Transfert aeroport"
    b_the = etree.SubElement(b_productf, 'Thematique')
#-------------------------------------------------------------------------THEMES
    nb_value = get_nb_value(source_file, "fr.prod.hotel.Themes")
    index = 0
    if nb_value:
        while index < nb_value :
            b_value = etree.SubElement(b_the, 'Value')
            b_value.text = get_value_2(source_file, "fr.prod.hotel.Themes", index)
            index += 1


    
    b_ess = etree.SubElement(b_productf, 'Essentiels')
    b_esse = etree.SubElement(b_ess, 'EssentielServices')
#-------------------------------------------------------------------------SERVICES
    nb_value = get_nb_value(source_file, "fr.prod.hotel.Services")
    if nb_value :
        index = 0
        while index < nb_value :
            b_value = etree.SubElement(b_esse, 'Value')
            b_value.text = get_value_2(source_file, "fr.prod.hotel.Services", index)
            index += 1

#-------------------------------------------------------------------------SPORT
    b_esss = etree.SubElement(b_ess, 'EssentielSports')
    nb_value = get_nb_value(source_file, "fr.prod.hotel.GroundSports")
    if nb_value :
        index = 0
        while index < nb_value :
            b_value = etree.SubElement(b_esss, 'Value')
            b_value.text = get_value_2(source_file, "fr.prod.hotel.GroundSports", index)
            index += 1


#-------------------------------------------------------------------------NAUTIQUE
    b_esssn = etree.SubElement(b_ess, 'EssentielSportsNautiques')
    nb_value = get_nb_value(source_file, "fr.prod.hotel.WaterSports")
    if nb_value :
        index = 0
        while index < nb_value :
            b_value = etree.SubElement(b_esssn, 'Value')
            b_value.text = get_value_2(source_file, "fr.prod.hotel.WaterSports", index)
            index += 1


#-------------------------------------------------------------------------PISCINE
    b_essp = etree.SubElement(b_ess, 'EssentielPiscine')
    nb_value = get_nb_value(source_file, "fr.prod.hotel.Pools")
    if nb_value :
        index = 0
        while index < nb_value :
            b_value = etree.SubElement(b_essp, 'Value')
            b_value.text = get_value_2(source_file, "fr.prod.hotel.Pools", index)
            index += 1

    b_essc = etree.SubElement(b_ess, "EssentielEnfants")
    b_value = etree.SubElement(b_essc, "Value")
    b_value.text = get_value(source_file, "fr.prod.hotel.welcomeForKids.title")
    b_value = etree.SubElement(b_essc, "Value")
    b_value.text = get_value(source_file, "fr.prod.hotel.welcomeForAdos.title")
            
    nb_chambre = nb_bedroom(source_file, "fr.prod.hotel.room.ProductCode")
    base_index = 0
    data = 0
    b_chams = etree.SubElement(b_productf, 'Chambres')
    while base_index < nb_chambre :
        b_cham = etree.SubElement(b_chams, 'Chambre')
        b_cdpc = etree.SubElement(b_cham, 'CodeProduitChambre')
        b_cdpc.text = chambre_desc(source_file,  data)
        data += 1
        b_c0 = etree.SubElement(b_cham,'CapaciteChambre')
        b_c0.text = chambre_desc(source_file, data)
        data += 1
        b_c1 = etree.SubElement(b_cham,'CodeChambre')
        b_c1.text = chambre_desc(source_file, data)
        data += 1
        b_c2 = etree.SubElement(b_cham,'TypeChambre')
        b_c2.text = chambre_desc(source_file, data)
        data += 1
        b_c3 = etree.SubElement(b_cham,'InfoChambre')
        b_c3.text = chambre_desc(source_file, data)
        data += 1
        b_c4 = etree.SubElement(b_cham,'DescriptionChambre')
        b_c4.text = chambre_desc(source_file, data)
        data += 1
        base_index = base_index + 1
    cde_iso = code_iso(source_file)
    cde_iso_n = code_iso_name(source_file)
 #   print("{};{};{}".format(source_file, cde_iso_n, cde_iso))
    b_geo = etree.SubElement(b_productf, 'GeoPolitical')
    b_loca = etree.SubElement(b_geo, 'Locality')
    b_loca.set('cityname', '')
    b_loca.set('countryCodeISO', cde_iso)
    b_loca.set('referencial', 'IATA')
    b_loca.set('code', iata_code(source_file))
    b_coun = etree.SubElement(b_geo, 'CountryCode')
    b_coun.set('name', '')
    b_coun.set('label', get_value(source_file, "country.name"))
    b_coun.set('ttlabel', '')
    b_rcod = etree.SubElement(b_geo, 'RegionCode')
    b_rcod.set('name', "")
    b_rcod.set('label', get_value(source_file, "region.name"))
    b_rcod.set('ttlabel', '')
    b_pcod = etree.SubElement(b_geo, 'PlaceCode')
    b_pcod.set('name',"")
    b_pcod.set('label', get_value(source_file, "place.name"))
    b_pcod.set('ttlabel', '')
    b_pcod.set('nurvislabel','')
    b_pcod.set('airport','')
    b_gps = etree.SubElement(b_geo, 'GPS')
    b_gps.set('Longitude', longitude_data(source_file, "gps.geotag"))
    b_gps.set('Latitude',  latitude_data(source_file, "gps.geotag"))
    b_gps.text = get_value(source_file, "gps.geotag")
    b_adress = etree.SubElement(b_geo, 'Address')
    b_adress.text = get_value(source_file, "hotel.address")
    b_rati = etree.SubElement(b_productf, 'PointsOfReference')
    b_offi = etree.SubElement(b_rati, 'Club')
    club = get_value(source_file, "fr.prod.hotel.label.club")
    if club:
        if len(club) > 1:
            b_offi.text = "Yes"
    else:
        b_offi.text = "No"
    b_offi = etree.SubElement(b_rati, 'Label')
    b_offi = etree.SubElement(b_rati, 'Logo')
    b_offi = etree.SubElement(b_rati, 'Wellness-type')
    b_offi = etree.SubElement(b_rati, 'Client-Award')
    b_offi = etree.SubElement(b_rati, 'Novelty')
    b_offi.text = get_value(source_file, "fr.prod.hotel.novelty")
    b_offi = etree.SubElement(b_rati, 'ProductType')
    if b_value0.text == "SEJOUR":
        b_offi.text = 'Sejour'
    if b_value0.text == "CLUB":
        b_offi.text = 'Club'
    b_offi = etree.SubElement(b_rati, 'Formula')
    b_offi.text = 'Voyage individuel'
#-------------------------------------------------------------GEO-----------------------------
    b_links = etree.SubElement(b_productf, "Links")

    b_link = etree.SubElement(b_links, "Link")
    b_link.set("Type", "YouTube")
    b_link.text = get_value(source_file, "attr.1289")
    b_link = etree.SubElement(b_links, "Link")
    b_link.set("Type", "YouTubeJET")
    b_link.text = get_value(source_file, "attr.1288")
    
    
    if check_type == "Club" :
        b_link = etree.SubElement(b_links, "Link")
        b_link.set("Type", "Extra Service Pack")
        b_link.set("Id", "PACK_ACTIV")
        b_name = etree.SubElement(b_link, "Name")
        b_name.text = get_value(source_file, "fr.prod.hotel.pack.active.title")
        b_desc = etree.SubElement(b_link, "Description")
        b_desc.text = get_value(source_file, "attr.1246")
    
        b_link = etree.SubElement(b_links, "Link")
        b_link.set("Type", "Extra Service Pack")
        b_link.set("Id", "PACK_DECOUV")
        b_name = etree.SubElement(b_link, "Name")
        b_name.text = get_value(source_file, "fr.prod.hotel.pack.discover.title")
        b_desc = etree.SubElement(b_link, "Description")
        b_desc.text = get_value(source_file, "attr.1247")

    
        b_link = etree.SubElement(b_links, "Link")
        b_link.set("Type", "Extra Service Pack")
        b_link.set("Id", "PACK_BIEN_ETRE")
        b_name = etree.SubElement(b_link, "Name")
        b_name.text = get_value(source_file, "fr.prod.hotel.pack.wellness.title")
        b_desc = etree.SubElement(b_link, "Description")
        b_desc.text = get_value(source_file, "attr.1245")

    
        b_link = etree.SubElement(b_links, "Link")
        b_link.set("Type", "Extra Service Pack")
        b_link.set("Id", "PACK_GOLD")
        b_name = etree.SubElement(b_link, "Name")
        b_name.text = get_value(source_file, "Pack Gold")
        b_desc = etree.SubElement(b_link, "Description")
        b_desc.text = get_value(source_file, "attr.1259")
        




    
    b_link = etree.SubElement(b_links, "Link")
    b_link.set("Type", "FicheBienvenue")
    b_obj = etree.SubElement(b_link, "ObjectName")
    b_obj.text = stocking_data(geo_file(source_file), "ObjectName")
    b_objt = etree.SubElement(b_link, "StandardObjectType")
    b_objt.text = stocking_data(geo_file(source_file), "StandardObjectType")
    b_com = etree.SubElement(b_link, "CommercialStepId")
    b_com.text = stocking_data(geo_file(source_file), "CommercialStepId")
    b_par = etree.SubElement(b_link, "ParentId")
    b_par.text = stocking_data(geo_file(source_file), "ParendId")
    b_sea = etree.SubElement(b_link, "Season")
    b_sea.text = get_value(geo_file(source_file), "backend.seasonCode")
    c_cont = etree.SubElement(b_link, "Country")
    c_cont.text = get_value(geo_file(source_file), "country.name")
    c_int = etree.SubElement(b_link, "Intro")
    c_int.text = get_value(geo_file(source_file), "be.country.introText")
    c_fot = etree.SubElement(b_link, "FormalitiesTitle")
    c_fot.text = "Formalités"
    c_form = etree.SubElement(b_link, "Formalities")
    c_form.text = get_value(geo_file(source_file), "be.geo.country.travelFormalities")
    c_het = etree.SubElement(b_link, "HealthTitle")
    c_het.text = get_value(geo_file(source_file), "be.geo.country.vaccinsAndHealth.title")
    c_hea = etree.SubElement(b_link, "Health")
    c_hea.text = get_value(geo_file(source_file), "be.geo.country.vaccinsAndHealth")
    c_ctp = etree.SubElement(b_link, "CotePratiqueTitre")
    c_ctp.text = get_value(geo_file(source_file), "be.geo.country.discoverPracInfo.title")
    c_ct = etree.SubElement(b_link, "CotePratique")
    c_ct.text = get_value(geo_file(source_file), "be.geo.country.PracticalInfo")
    b_assets = etree.SubElement(b_productf, 'DigitalAssets')

    nb_asset = nb_images(source_file, "DigitalAsset")
    data_nb = 0
    while nb_asset > 0 :
        b_asset = etree.SubElement(b_assets, 'DigitalAsset')
        b_asset.set('StepId', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('IsMainImage', data_image(source_file, data_nb))
        data_nb += 1
        tmp = data_image(source_file, data_nb)
        tmp_2 = code_image(tmp)
        b_asset.set('AssetType', tmp_2)
        data_nb += 1
        b_asset.set('AssetName', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('MimeType', "image/jpeg")
        b_asset.set('StandardAssetType', "IMAGE")
        b_asset.set('Revision', "")
        b_asset.set('width', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('height', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('WebSelection', "INCONNUE")
        b_alt = etree.SubElement(b_asset, 'AltText')
        b_cap = etree.SubElement(b_asset, 'Caption')
        nb_asset = nb_asset - 1

    b_rati = etree.SubElement(b_productf, 'Ratings')
    b_offi = etree.SubElement(b_rati, 'OfficialRating')
    b_offi.text = get_value(source_file, "hotel.ratings.officalRating")
    b_offi = etree.SubElement(b_rati, 'TCFRating')
    b_offi.text = get_value(source_file, "fr.prod.hotel.rating.ownRating")
    b_offi = etree.SubElement(b_rati, 'WebRating')
    ratting = get_value(source_file, "hotel.ratings.officalRating")
    if ratting:
        rat = ratting.split(' ')
        if len(rat[0]) == 1 :
            b_offi.text = rat[0]+".0"
        if len(rat[0]) > 1 :
            b_offi.text = rat[0]

    b_offer = etree.SubElement(b_productf, "Offers")
    
    b_spe = etree.SubElement(b_offer, "Special")
    b_off = etree.SubElement(b_spe, "ExclusiveOffers")
    b_speoff = etree.SubElement(b_spe, "SpecialOffersText")
    b_speoff.text = get_value(source_file, "fr.prod.hotel.specialOffers.text")# + get_value(source_file, "attr.1384")
    b_excluoff = etree.SubElement(b_spe, "ExclusiveOffersNote")
    b_excluoff.text = get_value(source_file, "fr.prod.hotel.specialOffers.note")
    b_wedding = etree.SubElement(b_offer, "Wedding")
    b_wed_t = etree.SubElement(b_wedding, "WeddingTitle")
    b_wed_t.text = get_value(source_file, "fr.prod.hotel.web.wedding.conditions.title")
    b_wed_t = etree.SubElement(b_wedding, "WeddingCondition")
    b_wed_t.text = get_value(source_file, "fr.prod.hotel.web.wedding.conditions.text")
    
    b_prict = etree.SubElement(b_offer, "PriceFrom")
    b_dan = etree.SubElement(b_prict, "DaysAndNights")
    b_dan.text = get_value(source_file, "fr.prod.hotel.pricesFrom.daysAndNights")
    b_price = etree.SubElement(b_prict, "Price")
    b_price.text = get_value(source_file, "fr.prod.hotel.pricesFrom")
    b_numprice = etree.SubElement(b_prict, "NumericalPrice")
    b_numprice.text = get_value(source_file, "")
    b_board = etree.SubElement(b_prict, "Board")
    b_board.text= get_value(source_file, "fr.prod.hotel.pricesFrom.board")
    b_note = etree.SubElement(b_prict, "Note")
    b_note.text= ""

    b_dep = etree.SubElement(b_prict, "Departure")

    b_lsea = etree.SubElement(b_offer, "LowSeason")
    b_lsprice = etree.SubElement(b_lsea, "Price")
    b_lnumprice = etree.SubElement(b_lsea, "NumericalPrice")
    b_ltitle = etree.SubElement(b_lsea, "Title")

    b_hsea = etree.SubElement(b_offer, "HighSeason")
    b_hsprice = etree.SubElement(b_hsea, "Price")
    b_hnumprice = etree.SubElement(b_hsea, "NumericalPrice")
    b_htitle = etree.SubElement(b_hsea, "Title")
    b_htitle.text = get_value(source_file, "fr.prod.hotel.pricesFrom.highSeasonTitle")

    try:
        name_file = init_name(source_file, get_value(source_file, "backend.productCode"), get_value(source_file, "fr.prod.hotel.productType"))
        with open(name_file,'w') as fichier:
            fichier.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            fichier.write(etree.tostring(b_base,pretty_print=True).decode('utf-8'))
        
    except IOError:
        print('Problème rencontré lors de l\'écriture ...')
        exit(1)

writting_XML(sys.argv[1])


