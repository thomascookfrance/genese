import os
import sys
from lxml import etree

country_file = "/srv/Software/PROG_GENESE/PROGRAMMES/XML/CONFIG/Countries.xml"
ref_images = "/srv/Software/PROG_GENESE/PROGRAMMES/XML/CONFIG/Code_images.xml"



def     stocking_data(source_file, balise):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""
    
    for elem in root.iter(balise) :
        if elem is not None:
            new_elem = elem.text
        else :
            new_elem = "X"
    return new_elem


def     stocking_value(source_file, balise, attrib):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""
    
    for elem in root.iter(balise):
        if elem is not None:
            new_value = elem.get(attrib)
        else:
            new_elem = "X"
    return new_value
    

def     init_name(source_file, code, type_p) :

    list = source_file.split('.')
    name = 'JETN.' + type_p + '.' + code + '.' + list[3]
    return (name)


def     attrib_data(source_file, code, index):

    xml = etree.parse(source_file)
    for it in xml.xpath("/StepMessage/Product/Topics/Topic/Paragraph"):
        if (it.get("Code")) == code:
            data = it.getchildren()
            return data[index].text

        
def     get_value(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Value"):
        if var == elem.get("AttributeID"):
            if elem.text:
                return(elem.text)
            else:
                return(None)


def     get_value_2(source_file, var, index):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Multivalue"):
        if (elem.get("AttributeID")) == var:
            data = elem.getchildren()
            return (data[index].text)

def     get_nb_value(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Multivalue"):
        if (elem.get("AttributeID")) == var:
            data = len(elem.getchildren())
            return (data)
        
def     longitude_data(source_file, balise):

    longitude = ""
    base = get_value(source_file, balise)
    if len(base) > 8:
        l_1 = base.split('(')
        l_1b = l_1[1]
        l_2 = l_1b[:-1]
        longitude = l_2.split(' ')
        return longitude[0]
    else:
        return (base)

    
def     latitude_data(source_file, balise):

    latitude = ""
    base = get_value(source_file, balise)
    if len(base) > 8:
        l_1 = base.split('(')
        l_1b = l_1[1]
        l_2 = l_1b[:-1]
        latitude = l_2.split(' ')
        return latitude[1]
    else:
        return (base)


def     code_iso(source_file):

    src = get_value(source_file, "country.name")
    if src :
        data = src.upper()
        tree = etree.parse(country_file)
        root = tree.getroot()
        for country in root.findall('Country'):
            name = country.get('countryName')
            code = country.get('codeISO')
            if data in name:
                return code
    else:
        return(None)


def     code_iso_name(source_file):

    src = get_value(source_file, "country.name")
    data = src.upper()
    return data



    
def     nb_bedroom(source_file, var):

    nb = 0
    with open(source_file, 'r') as data:
        for line in data:
            if var in line:
                nb = nb + 1
    return nb


def     nb_images(source_file, var):

    nb = 0
    with open(source_file, 'r') as data:
        for line in data:
            if var in line:
                nb = nb + 1
    return nb


def     legend_image(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Assets/DigitalAsset/Caption"):
        listing.append(elem.text)
    return (listing[index])
    
#----------------------------------------------------------------GEO


def     geo_file(source_file):

    id = get_value(source_file, "fr.prod.roundtrip.destidescriptionref")
    if id:
        filename = id + ".xml"
        return filename

#--------------------------------------------------------------

def             chambre_desc(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Chambres/Chambre"):
        data = elem.getchildren()
        listing.append(data[0].text)
        listing.append(data[1].text)
        listing.append(data[2].text)
        listing.append(data[3].text)
        listing.append(data[4].text)
        listing.append(data[5].text)
    return (listing[index])


def             circuit_desc(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Days/Day"):
        data = elem.getchildren()
        listing.append(data[0].text)
        listing.append(data[1].text)
        
    return(listing[index])


def             circuit_images(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Days/Day/Assets/DigitalAsset"):
        listing.append(elem.get("StepId"))
    return (listing[index])

def     check_image(source_file):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Days/Day/Assets/DigitalAsset"):
        listing.append(elem.get("StepId"))
    data = len(listing)
    return data


def     data_image(source_file, index):

    listing = []
    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Assets/DigitalAsset"):
        listing.append(elem.get("StepId"))
        listing.append(elem.get("IsMainImage"))
        listing.append(elem.get("AssetType"))
        listing.append(elem.get("AssetName"))
        listing.append(elem.get("width"))
        listing.append(elem.get("height"))
    return (listing[index])


def     image_nb(source_file):

    xml = etree.parse(source_file)
    result = len(xml.xpath("/SyspadMessage/Product/Assets/DigitalAsset"))
    return (result)


def     get_value_3(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Multivalue"):
        if (elem.get("AttributeID")) == var:
            data = len(elem.getchildren())
            return (data)

        
def     geo_file(source_file):

    id = get_value(source_file, "fr.prod.roundtrip.destidescriptionref")
    filename = id + ".xml"
    return filename


def     nb_stage(source_file):

    xml = etree.parse(source_file)
    result = len(xml.xpath("/SyspadMessage/Product/Days/Day"))
    return (result)


def     contrat_letter(source_file):

    list = []
    list_contrat = []
    ekms_code = get_value(source_file, "backend.productCode")
    nb_value = get_nb_value(source_file, "backend.subProductCodes")
    if nb_value:
        index = 0
        while index < nb_value :
            data = get_value_2(source_file, "backend.subProductCodes", index)
            list.append(data)
            index += 1
        for elem in list:
            cc = ekms_code + elem
            list_contrat.append(cc)
        return(list_contrat)
    


def     code_image(src):

    tree = etree.parse(ref_images)
    root = tree.getroot()
    for elem in root.findall('Code'):
        name = elem.get('codeNumber')
        code_i = elem.get('codeImage')
        if src in name:
            return(code_i)
                                                    
    

def     writting_XML(source_file):

    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++MANIFEST
    b_base = etree.Element('StepMessage')
    b_manifest = etree.SubElement(b_base,'Manifest')
    b_datasource = etree.SubElement(b_manifest, 'DataSource')
    b_datasource.text = stocking_data(source_file, 'DataSource')
    b_commercial = etree.SubElement(b_manifest, 'CommercialStepId')
    b_commercial.text = stocking_data(source_file, 'CommercialStepId')
    b_common = etree.SubElement(b_manifest, 'CommonStepId')
    b_common.text = stocking_data(source_file, 'CommonStepId')
    b_workspace = etree.SubElement(b_manifest, 'WorkspaceId')
    b_workspace.text = stocking_data(source_file, 'WorkspaceId')
    b_objectname = etree.SubElement(b_manifest, 'ObjectName')
    b_objectname.text = stocking_data(source_file, 'ObjectName')
    b_objecttype = etree.SubElement(b_manifest, 'ObjectType')
    b_objecttype.text = stocking_data(source_file, 'ObjectType')
    b_typeficheproduit = etree.SubElement(b_manifest, 'TypeFicheProduit')
    b_typeficheproduit.text = "CIRCUIT"
    b_typevoyage = etree.SubElement(b_manifest, 'TypeVoyage')
    b_value0 = etree.SubElement(b_typevoyage, 'Value')
    value = get_value_2(source_file, 'fr.prod.roundtrip.subtype', 0)
    if "Circuit" in value:
        b_value0.text = "CIRCUIT"
    else:
        b_value0.text= "CROISIERE"
    b_parentid = etree.SubElement(b_manifest, 'ParentId')
    b_parentid.text = b_common.text
    b_revision = etree.SubElement(b_manifest, 'Revision')
    b_revision.text = stocking_data(source_file, 'Revision')
    b_langue = etree.SubElement(b_manifest, 'LanguageCode')
    b_langue.text = stocking_data(source_file, 'LanguageCode')
    b_path = etree.SubElement(b_manifest, 'FilePath')
    b_path.text = source_file
    b_name = etree.SubElement(b_manifest, 'FileName')
    b_name.text = "JETN." + source_file
    b_srcf = etree.SubElement(b_manifest, 'SourceFileName')
    b_srcf.text = source_file
    b_asset = etree.SubElement(b_manifest, 'AssetPath')
    b_asset.text = "JET/IMAGES"
    b_status = etree.SubElement(b_manifest, 'Status')
    if "Active" in stocking_data(source_file, "Status"):
        b_status.text = "enabled"
    else:
        b_status.text = "Obselete"
    b_productf = etree.SubElement(b_base, 'Product')
    b_back = etree.SubElement(b_productf, 'BackendCodes')    
    b_reserv = etree.SubElement(b_back, 'ReservationSystem')
    b_reserv.set('hotel', "")
    b_reserv.set('package', "")
    b_reserv.set('location', "")
    b_reserv.set('camping', "")
    b_reserv.text = get_value(source_file,"backend.backendSystem")
    b_prodbook = etree.SubElement(b_back, 'ProductBookingsCode')
    data = contrat_letter(source_file)
    if data:
        str_1 = ";".join(str(x) for x in data)
        b_prodbook.text = str_1
    else:
        b_prodbook.text = get_value(source_file, "backend.productCode")
    b_market = etree.SubElement(b_back, 'Market')
    b_market.text = "TC-FR"
    b_brandcode = etree.SubElement(b_back, 'BrandCode')
    b_brandcode.text = "JET"
    b_ss = etree.SubElement(b_back, 'Season')
    b_ss.text = get_value(source_file, "backend.seasonCode")
    b_channel = etree.SubElement(b_back, 'ChannelName')
    b_channel.text = "Compos Juliot Jet tours"
    b_commer = etree.SubElement(b_productf, 'CommercialTexts')
    b_producti = etree.SubElement(b_commer, 'ProductTitle')
    b_producti.text = get_value(source_file, "roundtrip.publishedName")
    b_slo = etree.SubElement(b_commer, 'Slogan')
    b_introtext = etree.SubElement(b_commer, 'IntroText')
    b_introtext.text = get_value(source_file, "fr.prod.roundtrip.introtext")
    b_topics = etree.SubElement(b_productf, 'Topics')
    b_topic = etree.SubElement(b_topics, 'Topic')
    b_topic.set('Code', 'FACILITIES')
    b_parag = etree.SubElement(b_topic, 'Paragraph')
    b_parag8 = etree.SubElement(b_topic, 'Paragraph')
    b_parag8.set('Id', 'NOS_PRIX_COMPRENNENT')
    b_title = etree.SubElement(b_parag8, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.inclInPriceLegal.title")
    b_body = etree.SubElement(b_parag8, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.inclInPriceLegal")
    b_parag9 = etree.SubElement(b_topic, 'Paragraph')
    b_parag9.set('Id', 'NOS_PRIX_NE_COMPRENNENT_PAS')
    b_title = etree.SubElement(b_parag9, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.notInclInPrice.title")
    b_body = etree.SubElement(b_parag9, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.notinclInPrice")
    b_parag10 = etree.SubElement(b_topic, 'Paragraph')
    b_parag10.set('Id', 'HEBERGEMENT')
    b_title = etree.SubElement(b_parag10, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.lodging.title")
    b_body = etree.SubElement(b_parag10, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.lodging")
    b_parag11 = etree.SubElement(b_topic, 'Paragraph')
    b_parag11.set('Id', 'VOYAGEZ_MALIN')
    b_title = etree.SubElement(b_parag11, 'Title')
    b_title.text = get_value(source_file, "attr.1191")
    b_body = etree.SubElement(b_parag11, 'Body')
    b_body.text = get_value(source_file, "attr.1193")

    b_body = etree.SubElement(b_parag11, 'Body')
    b_body.text = get_value(source_file, "attr.1195")

    b_parag24 = etree.SubElement(b_topic, 'Paragraph')
    b_parag24.set('Id', 'JOIGNEZ_VOUS_A_LA_FETE')
    b_title = etree.SubElement(b_parag24, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.bestTimeToVisit.title")
    b_body = etree.SubElement(b_parag24, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.bestTimeToVisit")

    
    b_parag12 = etree.SubElement(b_topic, 'Paragraph')
    b_parag12.set('Id', 'VOUS_ALLER_AIMER')
    b_title = etree.SubElement(b_parag12, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.strongPoints.title")
    b_body = etree.SubElement(b_parag12, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.strongPoints")
    b_parag13 = etree.SubElement(b_topic, 'Paragraph')
    b_parag13.set('Id', 'INFOS_VOYAGE')
    b_title = etree.SubElement(b_parag13, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.extraInfo.title")
    b_body = etree.SubElement(b_parag13, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.extraInfo")
    b_parag14 = etree.SubElement(b_topic, 'Paragraph')
    b_parag14.set('Id', 'PRINCIPAUX_SITES_VISITES')
    b_title = etree.SubElement(b_parag14, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.mainplaces.title")
    b_body = etree.SubElement(b_parag14, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.mainplaces.text")
    b_parag15 = etree.SubElement(b_topic, 'Paragraph')
    list_data = []
    data_src = ""
    b_parag15.set('Id', 'PARTEZ_BIEN_INFORMES')
    if get_value(source_file, "attr.1182") is not None:
        b_title = etree.SubElement(b_parag15, 'Title')
        b_title.text = get_value(source_file, "attr.1182")
    if get_value(source_file, "attr.1183") is not None:
        data_0 = get_value(source_file, "attr.1183")
        list_data.append(data_0)
    if get_value(source_file, "attr.1184") is not None:
        data_1 = get_value(source_file, "attr.1184")
        list_data.append(data_1)
    if get_value(source_file, "attr.1185") is not None:
        data_2 =get_value(source_file, "attr.1185")
        list_data.append(data_2)
    if get_value(source_file, "attr.1186") is not None:
        data_3 = get_value(source_file, "attr.1186")
        list_data.append(data_3)
    if get_value(source_file, "attr.1187") is not None:
        data_4 = get_value(source_file, "attr.1187")
        list_data.append(data_4)
    if get_value(source_file, "attr.1188") is not None:
        data_5 = get_value(source_file, "attr.1188")
        list_data.append(data_5)
    for elem in list_data:
        t = "<br />"
        data = elem + t
        data_src += data
    b_body = etree.SubElement(b_parag15, "Body")
    b_body.text = data_src
    b_tobloc = etree.SubElement(b_topics, 'Topic')
    b_tobloc.set('Code', 'BLOCKS')

    nb_days = nb_stage(source_file)
    nb = check_image(source_file)
    index = 0
    inc = 1
    i = 0
    while i < nb_days :
        b_paracir = etree.SubElement(b_tobloc, 'Paragraph')
        b_paracir.set('Code', circuit_desc(source_file, index))
        index += 1
        #if nb > 0:
         #   b_paracir.set('DigitalAsset', circuit_images(source_file, i))
        #else:
        b_paracir.set('DigitalAsset', "")
        b_paracir.set('Id', "STAGE")
        b_paracir.set('Day', str(inc))
        inc += 1
        b_body = etree.SubElement(b_paracir, 'Body')
        b_body.text = circuit_desc(source_file, index)
        index += 1
        i = i + 1

    b_parag16 = etree.SubElement(b_topics, 'Topic')
    b_parag16.set('Code', 'EXTENSION')
    b_oar = etree.SubElement(b_parag16, 'Paragraph')
    b_oar.set('Code', 'Extension')
    b_oar.set('Id', 'ADDITIONALS')
    b_title = etree.SubElement(b_oar, 'Title')
    b_title.text = get_value(source_file, "fr.prod.roundtrip.extension.title")
    b_body = etree.SubElement(b_oar, 'Body')
    b_body.text = get_value(source_file, "fr.prod.roundtrip.extension")
    b_links = etree.SubElement(b_productf, "Links")
    b_link = etree.SubElement(b_links, "Link")
    b_link.set("Type", "FicheBienvenue")
    b_obj = etree.SubElement(b_link, "ObjectName")
    b_obj.text = stocking_data(geo_file(source_file), "ObjectName")
    b_objt = etree.SubElement(b_link, "StandardObjectType")
    b_objt.text = stocking_data(geo_file(source_file), "StandardObjectType")
    b_com = etree.SubElement(b_link, "CommercialStepId")
    b_com.text = stocking_data(geo_file(source_file), "CommercialStepId")
    b_par = etree.SubElement(b_link, "ParentId")
    b_par.text = stocking_data(geo_file(source_file), "ParendId")
    b_sea = etree.SubElement(b_link, "Season")
    b_sea.text = get_value(geo_file(source_file), "backend.seasonCode")
    c_cont = etree.SubElement(b_link, "Country")
    c_cont.text = get_value(geo_file(source_file), "country.name")
    c_int = etree.SubElement(b_link, "Intro")
    c_int.text = get_value(geo_file(source_file), "be.country.introText")
    c_fot = etree.SubElement(b_link, "FormalitiesTitle")
    c_fot.text = "Formalités"
    c_form = etree.SubElement(b_link, "Formalities")
    c_form.text = get_value(geo_file(source_file), "be.geo.country.travelFormalities")
    c_het = etree.SubElement(b_link, "HealthTitle")
    c_het.text = get_value(geo_file(source_file), "be.geo.country.vaccinsAndHealth.title")
    c_hea = etree.SubElement(b_link, "Health")
    c_hea.text = get_value(geo_file(source_file), "be.geo.country.vaccinsAndHealth")
    c_ctp = etree.SubElement(b_link, "CotePratiqueTitre")
    c_ctp.text = get_value(geo_file(source_file), "be.geo.country.discoverPracInfo.title")
    c_ct = etree.SubElement(b_link, "CotePratique")
    c_ct.text = get_value(geo_file(source_file), "be.geo.country.PracticalInfo")
        
    b_dep = etree.SubElement(b_productf, 'DepartGarantis')
    b_dep.text = get_value(source_file, "fr.prod.roundtrip.departures.guaranteed")
    b_annul = etree.SubElement(b_productf, 'PolitiqueAnnulation')
    b_annul.text = get_value(source_file, "fr.prod.roundtrip.cancelation.policy")
    b_picdep = etree.SubElement(b_productf,'PictoDepartGarantis')
    if len( get_value(source_file, "fr.prod.roundtrip.departures.guaranteed")) > 5:
        b_picdep.text = "Oui"
    else:
        b_picdep.text = "Non"
    b_ptc = etree.SubElement(b_productf,'PictoTropheClient')
    b_ptc.text = get_value(source_file, "fr.prod.roundtrip.clientAward")
    b_pcp = etree.SubElement(b_productf,'PictoCircuitPrivatisable')
    b_pcp.text = get_value(source_file, "fr.roundtrip.text.optionPrivate")
    b_pgp = etree.SubElement(b_productf,'PictoGuideParis')
    b_tb = etree.SubElement(b_productf,'TitreBarometre')
    b_tb.text = get_value(source_file, "fr.prod.roundtrip.ratings.title")
    b_vc = etree.SubElement(b_productf,'VideoCircuit')
    b_ap = etree.SubElement(b_productf,'AvisProfessionel')
    b_ap.text = get_value(source_file, "fr.prod.roundtrip.expertadvice")
    b_np = etree.SubElement(b_productf,'NomProfessionel')
    b_np.text = get_value(source_file, "fr.prod.roundtrip.expert.name")
    b_hc = etree.SubElement(b_productf,'HebergementContractuel')
    b_hc.text = get_value(source_file, "fr.prod.roundtrip.lodging.note")
    b_oe = etree.SubElement(b_productf,'OffreExtension')
    b_tpext = etree.SubElement(b_oe,'TitrePrincipalExtension')
    b_tpext.text = get_value(source_file, "fr.prod.roundtrip.combination.title")
    b_ofre1 = etree.SubElement(b_oe,'Offre1')
    b_jne = etree.SubElement(b_ofre1,'JoursNuitsExtension1')
    b_dte = etree.SubElement(b_ofre1,'DestinationExtension1')
    b_de = etree.SubElement(b_ofre1,'DescriptifExtension1')
    b_ofre2 = etree.SubElement(b_oe,'Offre2')
    b_jne2 = etree.SubElement(b_ofre2,'JoursNuitsExtension2')
    b_dte2 = etree.SubElement(b_ofre2,'DestinationExtension2')
    b_de2 = etree.SubElement(b_ofre2,'DescriptifExtension2')
    b_cons = etree.SubElement(b_productf, 'ConceptHotel')
    b_cons.text = get_value(source_file, "fr.prod.hotel.label")
    b_the = etree.SubElement(b_productf, 'Thematique')
    b_vth = etree.SubElement(b_the, 'Value')
    b_vth.text = get_value(source_file, "fr.prod.hotel.wellness.title")
    b_vth2 = etree.SubElement(b_the, 'Value')
    b_vth2.text = get_value_2(source_file, "fr.prod.hotel.Themes", 1)
    b_ess = etree.SubElement(b_productf, 'Essentiels')
    b_esse = etree.SubElement(b_ess, 'EssentielServices')
    b_essi_v = etree.SubElement(b_esse, 'Value')
    b_essi_v.text = get_value_2(source_file, "fr.prod.hotel.Services", 0)
    b_esss = etree.SubElement(b_ess, 'EssentielSports')
    b_esss_v = etree.SubElement(b_esss, 'Value')
    b_esss_v.text = get_value_2(source_file, "fr.prod.hotel.GroundSports", 0)
    b_esssn = etree.SubElement(b_ess, 'EssentielSportsNautiques')
    b_esssn_v = etree.SubElement(b_esssn, 'Value')
    b_esssn_v.text = get_value_2(source_file, "fr.prod.hotel.WaterSports", 0)
    b_essp = etree.SubElement(b_ess, 'EssentielPiscine')
    b_essp_v = etree.SubElement(b_essp, 'Value')
    b_essp_v.text = get_value_2(source_file, "fr.prod.hotel.Pools", 0)
    nb_chambre = nb_bedroom(source_file, "fr.prod.hotel.room.ProductCode")
    b_geo = etree.SubElement(b_productf, 'GeoPolitical')
    b_loca = etree.SubElement(b_geo, 'Locality')
    cde_iso = code_iso(source_file)
    cde_name = code_iso_name(source_file)
#    print("{};{};{}".format(source_file, cde_name, cde_iso))
    b_loca.set('cityName', '')
    b_loca.set('countryCodeISO', code_iso(source_file))
    b_loca.set('referencial', '')
    b_loca.set('code', '')
    b_coun = etree.SubElement(b_geo, 'CountryCode')
    b_coun.set('name', '')
    b_coun.set('label', get_value(source_file, "country.name"))
    b_coun.set('ttlabel', get_value(source_file, "country.name"))
    str_code_sub = get_value_2(source_file, "fr.prod.roundtrip.subtype", 0)
    str_country = get_value(source_file, "country.name")
    allt = str_code_sub + " " + str_country
    b_rcod = etree.SubElement(b_geo, 'RegionCode')
    b_rcod.set('name', allt)
    b_rcod.set('label', allt)
    b_rcod.set('ttlabel', allt)
    b_pcod = etree.SubElement(b_geo, 'PlaceCode')
    b_pcod.set('name', allt)
    b_pcod.set('label', allt)
    b_pcod.set('ttlabel', allt)
    b_pcod.set('airport', '')
    b_gps = etree.SubElement(b_geo, 'GPS')
    b_gps.set('Longitude', "")
    b_gps.set('Latitude', "")
    b_adress = etree.SubElement(b_geo, 'Address')
    b_adress.text = get_value(source_file, "hotel.address")
    b_rati = etree.SubElement(b_productf, 'PointsOfReference')
    b_offi = etree.SubElement(b_rati, 'Club')
    b_offi = etree.SubElement(b_rati, 'Label')
    b_offi = etree.SubElement(b_rati, 'Logo')
    b_offi = etree.SubElement(b_rati, 'Wellness-type')
    b_offi = etree.SubElement(b_rati, 'Client-Award')
    b_offi.text = get_value(source_file, "fr.prod.roundtrip.clientAward")
    b_offi = etree.SubElement(b_rati, 'Novelty')
    b_offi = etree.SubElement(b_rati, 'ProductType')
    b_offi.text = get_value_2(source_file, 'fr.prod.roundtrip.subtype', 0)
    b_offi = etree.SubElement(b_rati, 'Formula')
    b_offi.text = 'Voyage individuel'
    b_offi = etree.SubElement(b_rati, 'RoundTripType')
    b_offi.text = get_value_2(source_file, "fr.prod.roundtrip.type", 0)
    b_offi = etree.SubElement(b_rati, 'Rating-Culture')
    b_offi.text = get_value(source_file, "fr.prod.roundtrip.rating.culture")
    b_offi = etree.SubElement(b_rati, 'Rating-Nature')
    b_offi.text = get_value(source_file, "fr.prod.roundtrip.rating.nature")
    b_offi = etree.SubElement(b_rati, 'Rating-Rhythm')
    b_offi.text = get_value(source_file, "fr.prod.roundtrip.rating.rhythm")
    b_offi = etree.SubElement(b_rati, 'Rating-Family')
    b_offi.text = get_value(source_file, "fr.prod.roundtrip.rating.family")
    b_assets = etree.SubElement(b_productf, 'DigitalAssets')
    nb_asset = nb_images(source_file, "AssetExported")

    nb_assets = image_nb(source_file)
    data_nb = 0
    legend = 0
    while nb_assets > 0 :
        b_asset = etree.SubElement(b_assets, 'DigitalAsset')
        b_asset.set('StepId', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('IsMainImage', data_image(source_file, data_nb))
        data_nb += 1
        tmp = data_image(source_file, data_nb)
        tmp_2 = code_image(tmp)
        b_asset.set('AssetType', tmp_2)
        data_nb += 1
        b_asset.set('AssetName', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('MimeType', "image/jpeg")
        b_asset.set('StandardAssetType', "IMAGE")
        b_asset.set('Revision', "")
        b_asset.set('width', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('height', data_image(source_file, data_nb))
        data_nb += 1
        b_asset.set('WebSelection', "INCONNUE")
        b_alt = etree.SubElement(b_asset, 'AltText')
        b_cap = etree.SubElement(b_asset, 'Caption')
        nb_assets = nb_assets - 1

    b_rati = etree.SubElement(b_productf, 'Ratings')
    b_offi = etree.SubElement(b_rati, 'OfficialRating')
    b_offi.text = get_value(source_file, "hotel.ratings.officalRating")
    b_offi = etree.SubElement(b_rati, 'OwnRating')
    b_offi = etree.SubElement(b_rati, 'TCFRating')
    b_offi.text = get_value(source_file, "fr.prod.hotel.rating.ownRating")
    b_offi = etree.SubElement(b_rati, 'WebRating')
    b_offer = etree.SubElement(b_productf, "Offers")
    b_spe = etree.SubElement(b_offer, "Special")
    b_off = etree.SubElement(b_spe, "ExclusiveOffers")
    b_speoff = etree.SubElement(b_spe, "SpecialOffersText")
    b_excluoff = etree.SubElement(b_spe, "ExclusiveOffersNote")
    b_wedding = etree.SubElement(b_offer, "Wedding")
    b_prict = etree.SubElement(b_offer, "PriceFrom")
    b_dan = etree.SubElement(b_prict, "DaysAndNights")
    b_dan.text = get_value(source_file, "fr.prod.roundtrip.pricesFrom2.daysNights")
    b_price = etree.SubElement(b_prict, "Price")
    b_price.text = get_value(source_file, "fr.prod.roundtrip.pricesFrom2")
    b_numprice = etree.SubElement(b_prict, "NumericalPrice")
    b_numprice.text = get_value(source_file, "fr.prod.hotel.pricesFrom")
    b_board = etree.SubElement(b_prict, "Board")
    b_board.text= get_value(source_file, "fr.prod.hotel.pricesFrom.board")
    b_note = etree.SubElement(b_prict, "Note")
    b_note.text= get_value(source_file, "fr.prod.roundtrip.pricesFrom2.footnote")
    b_dep = etree.SubElement(b_prict, "Departure")
    b_lsea = etree.SubElement(b_offer, "LowSeason")
    b_lsprice = etree.SubElement(b_lsea, "Price")
    b_lnumprice = etree.SubElement(b_lsea, "NumericalPrice")
    b_ltitle = etree.SubElement(b_lsea, "Title")
    b_hsea = etree.SubElement(b_offer, "LowSeason")
    b_hsprice = etree.SubElement(b_hsea, "Price")
    b_hnumprice = etree.SubElement(b_hsea, "NumericalPrice")
    b_htitle = etree.SubElement(b_hsea, "Title")
    b_htitle.text = get_value(source_file, "fr.prod.hotel.pricesFrom.highSeasonTitle")

#    print("{},{},{}".format(source_file, get_value_2(source_file, 'fr.prod.roundtrip.subtype', 0), geo_file(source_file)))
    
    try:
        code_t = get_value(source_file, "backend.productCode")
        code_name = code_t[:6]
        name_file = init_name(source_file, code_name, get_value_2(source_file, 'fr.prod.roundtrip.subtype', 0))
        with open(name_file,'w') as fichier:
            fichier.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            fichier.write(etree.tostring(b_base,pretty_print=True).decode('utf-8'))
        
    except IOError:
        print('Problème rencontré lors de l\'écriture ...')
        exit(1)

writting_XML(sys.argv[1])
