import os
import sys
from lxml import etree

#--------------------------------------------------------CMI24
def     stocking_value(source_file, balise, attrib):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""

    for elem in root.iter(balise):
        if elem is not None:
            new_elem = elem.get(attrib)
        else:
            new_elem = ""
    return new_elem
                                    
#-------------------------------------------------------SYSPAD


def     get_value(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Value"):
        if var == elem.get("AttributeID"):
            if elem.text :
                return (elem.text)
            else:
                return ("")
                                


def     test(data):
    tree = etree.parse("/home/transfert-content/SI_STRUCT_V2/PROGRAMMES/XML/CONFIG/Countries.xml")
    root = tree.getroot()
    for country in root.findall('Country'):
        name = country.get('countryName')
        code = country.get('codeISO')
        if data in name:
            return code



def     get_value_conf_file(data):

    xml = etree.parse("Countries.xml")
    for elem in xml.xpath("/Countries/Country"):
        if country in elem.get("countryName"):
            print(elem.get("countryName"))
            if elem.text:
                return(elem.text)
            else:
                return("")



def     init(path):

    index = 0
    os.chdir(path)
    listing = os.listdir(path)
    for elem in listing:
        if ".xml" in elem:
            if "roundtrip.comm" in elem or "hotel.comm" in elem:
                data = get_value(elem, "country.name")
                if data:
                    data_2 = data.upper()
                    print("{};{};{}".format(elem, data_2, test(data_2)))


#test()
init("/home/transfert-content/SI_STRUCT_V2/SOURCES/XML/SYSPAD/")
