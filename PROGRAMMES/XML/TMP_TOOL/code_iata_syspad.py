import os
import sys
import requests
import json
from lxml import etree




def     get_value(source_file, var):

    xml = etree.parse(source_file)
    for elem in xml.xpath("/SyspadMessage/Product/Values/Value"):
        if var == elem.get("AttributeID"):
            if elem.text :
                return (elem.text)
            else:
                return ("")
                            


def     stocking_data(source_file, balise):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""

    for elem in root.iter(balise):
        if elem is not None:
            new_elem = elem.text
        else:
            new_elem = ""
    return new_elem




def     longi(source_file, balise):

    base = stocking_data(source_file, balise)
    if base is not None:
        l1 = base.split('(')
        l1b = l1[1]
        l2 = l1b[:-1]
        longi = l2.split(' ')
        return longi[0]


def     latt(source_file, balise):

    base = stocking_data(source_file, balise)
    if base is not None:
        l1 = base.split('(')
        l1b = l1[1]
        l2 = l1b[:-1]
        lattitude = l2.split(' ')
        return lattitude[1]



def     longitude_data(source_file, balise):

    base = get_value(source_file, balise)
    if len(base) > 8 :
        l_1 = base.split('(')
        l_1b = l_1[1]
        l_2 = l_1b[:-1]
        longitude = l_2.split(' ')
        return longitude[0]
    else:
        return(base)


def     latitude_data(source_file, balise):
    
    base = get_value(source_file, balise)
    if len(base) > 8:
        l_1 = base.split('(')
        l_1b = l_1[1]
        l_2 = l_1b[:-1]
        latitude = l_2.split(' ')
        return latitude[1]
    else:
        return(base)
    
            

    
def     init(path):
    
    ct_good = 0
    ct_bad = 0
    ct_inv = 0
    iata = ""
    os.chdir(path)
    list = os.listdir(path)
    for elem in list:
        if ".xml" in elem and "geo." not in elem and "hotel" in elem:
            try:
                lattitude = latitude_data(elem, "gps.geotag")
                longitude = longitude_data(elem, "gps.geotag")
                ekms = get_value(elem, "backend.productCode")
                lattt = str(lattitude)
                longit = str(longitude)
                r = requests.get("http://iata.thomascook.fr:8080/iata/nearest?lat=" + lattt + "&long=" + longit)
                if r:
                    rr = json.loads(r.text)
                    for cle, value in rr.items():
                        if "code" in cle:
                            iata = value
                            ct_good += 1
                    if iata:
                        print("CODE EKMS: {}, CODE IATA: {} ".format(ekms, iata))
            except:
                print(elem + " FILE ERROR")



init("/srv/SRC/SOURCES/XML/SYSPAD/")


