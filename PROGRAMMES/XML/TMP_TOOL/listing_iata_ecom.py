import os
import sys
import requests
import json
from lxml import etree


def     stocking_value(source_file, balise, attrib):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""

    for elem in root.iter(balise):
        if elem is not None:
            new_elem = elem.get(attrib)
        else:
            new_elem = ""
    return new_elem


def     stocking_data(source_file, balise):

    xml = etree.parse(source_file)
    root = xml.getroot()
    new_elem = ""

    for elem in root.iter(balise):
        if elem is not None:
            new_elem = elem.text
        else:
            new_elem = ""
    return new_elem




def     longi(source_file, balise):

    base = stocking_data(source_file, balise)
    if base is not None:
        if len(base) > 2:
            l1 = base.split('(')
            l1b = l1[1]
            l2 = l1b[:-1]
            longi = l2.split(' ')
            return longi[0]


def     latt(source_file, balise):

    base = stocking_data(source_file, balise)
    if base is not None:
        if len(base) > 2:
            l1 = base.split('(')
            l1b = l1[1]
            l2 = l1b[:-1]
            lattitude = l2.split(' ')
            return lattitude[1]

    

def     init(path):


    os.chdir(path)
    list = os.listdir(path)
    for elem in list:
        if ".xml" in elem:
            lattitude = latt(elem, "Coordinate")
            ekms_code = stocking_data(elem, 'ProductBookingsCode')
            country = stocking_data(elem, 'CountryCode')
            ville = stocking_value(elem, 'PlaceCode', 'label')
            region = stocking_value(elem, 'RegionCode', 'label')
            longitude = longi(elem, "Coordinate")
            lattt = str(lattitude)
            longit = str(longitude)
            if lattt is not None and longit is not None:
                r = requests.get("http://iata.thomascook.fr:8080/iata/nearest?lat=" + lattt + "&long=" + longit)
                if r:
                    rr = json.loads(r.text)
                    for cle, value in rr.items():
                        if "code" in cle:
                            iata = value
                    if iata:
                        print("{};{};{};{};{};{}".format(elem, ekms_code, country, region, ville, iata))
                else:
                    r_inv = requests.get("http://iata.thomascook.fr:8080/iata/nearest?lat=" + longit + "&long=" + lattt)
                    if r_inv :
                        rr = json.loads(r_inv.text)
                        for cle, value in rr.items():
                            if "code" in cle:
                                iata = value
                        if iata:
                            print("{};{};{};{};{};{}".format(elem, ekms_code, country, region, ville, iata))
            else:
                print("PAS DE CODE IATA")



init("/srv/SRC/SOURCES/XML/CMI24/")


